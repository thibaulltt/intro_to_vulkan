#ifndef VK_ENUMERATORS_HPP
#define VK_ENUMERATORS_HPP

#include "vk.hpp"
#include <vulkan/vulkan_core.h>

#ifndef VULKAN_CORE_H_
#	include <vulkan/vulkan_core.h>
#endif

namespace Vulkan {
namespace Enumerators {

	/// @brief Enumerates all extensions available for an instance, filtered by layer name.
	/// @param pLayerName The name of the layer to query extensions for. If nullptr is given, no filtering is performed.
	/// @return A vector which is either empty (no extensions are supported with the current filter), or contains the
	/// extension properties of each available extension for the instance.
	std::vector<VkExtensionProperties> instanceExtensionProperties(const char* pLayerName = nullptr);

	/// @brief Enumerates all extensions available for a device.
	/// @return A vector which is either empty (no extensions are supported), or contains the
	/// extension properties of each available extension for the device.
	std::vector<VkExtensionProperties> deviceExtensionProperties(const VkPhysicalDevice& physicalDevice, const char* pLayerName = nullptr);

	/// @brief Enumerates all layers available for an instance.
	/// @return A vector which is either empty (no layers are available), or contains the layer properties of each
	/// available layers for the instance.
	std::vector<VkLayerProperties> instanceLayerProperties();

	/// @brief Enumerates all the physical devices present on the current system.
	/// @return All of the physical devices available on the system.
	std::vector<VkPhysicalDevice> physicalDevices(VkInstance instance);

	/// @brief Enumerates all queue families and their properties for a given physical device.
	/// @return All the queue families for the given physical device.
	std::vector<VkQueueFamilyProperties> queueProperties(const VkPhysicalDevice& dev);

	/// @brief Enumerates all possible extensions for a physical device.
	std::vector<VkExtensionProperties> extensionProperties(const VkPhysicalDevice& dev);

	std::vector<VkSurfaceFormatKHR> surfaceFormats(const VkPhysicalDevice& dev, const VkSurfaceKHR& surf);
	std::vector<VkPresentModeKHR> presentModes(const VkPhysicalDevice& dev, const VkSurfaceKHR& surf);

}
}

#endif // VK_ENUMERATORS_HPP
