#include "device_filter.hpp"
#include "enumerators.hpp"
#include "vk.hpp"
#include "vk_strings.hpp"
#include <initializer_list>
#include <iterator>
#include <set>
#include <vulkan/vulkan_core.h>

#ifndef VULKAN_CORE_H_
#	include <vulkan/vulkan_core.h>
#endif

Vulkan::DeviceFilter::DeviceFilter(const std::vector<VkPhysicalDevice>& devices, const VkSurfaceKHR& surface) {
	// Shortcut for an iterator of this vector :
	typedef std::vector<VkPhysicalDevice>::const_iterator vec_it;

	bool printVals = false;

	// All devices have a score of 0 at the start :
	for (vec_it it = devices.begin(); it != devices.end(); ++it) {
		VkPhysicalDeviceProperties devProps = this->getPhysicalDeviceProperties(*it, printVals);
		VkPhysicalDeviceFeatures devFeats = this->getPhysicalDeviceFeatures(*it, printVals);
		QueueFamilies fams = this->getAvailableQueues(*it, surface, printVals);
		SwapchainSupportDetails dets = this->swapchainSupport(*it, surface);
		std::cerr << "Checking out device " << devProps.deviceName << "\n";
		this->physicalDevices.push_back({&(*it), devProps, devFeats, fams, dets, .0f});
	}
}

Vulkan::DeviceFilter::~DeviceFilter(void) {
	this->physicalDevices.clear();
}

Vulkan::DeviceFilter& Vulkan::DeviceFilter::addDeviceExtensions(std::initializer_list<const char*> ext) {
	for (const auto& c : ext) {
		this->extensions.push_back(c);
	}
	return *this;
}

Vulkan::DeviceFilter& Vulkan::DeviceFilter::addDeviceExtensions(std::vector<const char*> ext) {
	for (const auto& c : ext) {
		this->extensions.push_back(c);
	}
	return *this;
}

Vulkan::DeviceFilter& Vulkan::DeviceFilter::addSurfaceFormats(std::initializer_list<VkSurfaceFormatKHR> ext) {
	for (const auto& c : ext) {
		this->formats.push_back(c);
	}
	return *this;
}

Vulkan::DeviceFilter& Vulkan::DeviceFilter::addPresentModes(std::initializer_list<VkPresentModeKHR> ext) {
	for (const auto& c : ext) {
		this->presentMode.push_back(c);
	}
	return *this;
}

Vulkan::DeviceFilter& Vulkan::DeviceFilter::setWindowsize(VkExtent2D size) {
	this->winSize = size;
	return *this;
}

Vulkan::DeviceFilter::PhyDev& Vulkan::DeviceFilter::getBestDevice(std::vector<const char*> extensionsrequired) {
	this->suitableDevices.clear();

	// Filter for only dGPUs :
	std::copy_if(std::begin(this->physicalDevices), std::end(this->physicalDevices), std::back_inserter(this->suitableDevices),
			[this, &extensionsrequired](const PhyDev& dev) {
				bool isRightType = dev.props.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU or dev.props.deviceType == VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU;
				bool hasQueues = dev.queues.isValid();
				bool hasSurfaceModes = not dev.details.formats.empty() && not dev.details.presentMode.empty();
				bool hasRightSurfaces = this->hasAllSurfaceFormats(dev);
				bool hasAnisotropy = (dev.feats.samplerAnisotropy == VK_TRUE);

				// check for extension support :
				std::vector<VkExtensionProperties> props = Vulkan::Enumerators::extensionProperties(*dev.device);
				std::set<std::string> reqExt(std::begin(extensionsrequired), std::end(extensionsrequired));
				for (const auto& p : props) {
					//std::cerr << "[device_filter] Checking device \"" << dev.props.deviceName << "\" for extension \"" << p.extensionName << "\"...\n";
					reqExt.erase(p.extensionName);
				}
				bool hasExtensions = reqExt.empty();

				return isRightType && hasQueues && hasSurfaceModes && hasRightSurfaces && hasExtensions && hasAnisotropy;
			});

	if (this->suitableDevices.size() == 0) {
		std::cerr << "Error : cannot find any suitable GPUs. Returning first device encountered.\n";
		return this->physicalDevices[0];
	} else {
		std::cerr << "Device filter: returning device " << this->suitableDevices[1].props.deviceName << '\n';
		if (this->suitableDevices.size() > 1) {
			std::cerr << "There was more than one suitable device.\n";
		}
		return this->suitableDevices[1];
	}
}

VkPhysicalDeviceProperties Vulkan::DeviceFilter::getPhysicalDeviceProperties(const VkPhysicalDevice& dev, bool printValues) {
	VkPhysicalDeviceProperties deviceProperties = {};
	vkGetPhysicalDeviceProperties(dev, &deviceProperties);

	if (printValues) {
		// Get all values, and _then_ print them :
		std::string devName = (strlen(deviceProperties.deviceName) == 0) ? "{error : no name found}" : deviceProperties.deviceName;
		Vulkan::Version apiVer = Vulkan::getVulkanVersion(deviceProperties.apiVersion);
		Vulkan::Version driVer = Vulkan::getVulkanVersion(deviceProperties.driverVersion);
		std::string devType = Vulkan::Strings::getPhysicalDeviceType(deviceProperties.deviceType);

		std::cerr << "Properties of device \"" << devName << "\" are :\n";
		std::cerr << "\tapiVersion : " << apiVer[0] << '.' << apiVer[1] << '.' << apiVer[2] << "\n";
		std::cerr << "\tdriverVersion : " << driVer[0] << '.' << driVer[1] << '.' << driVer[2] << '\n';
		std::cerr << "\tvendorID : " << deviceProperties.vendorID << ", deviceID : " << deviceProperties.deviceID << '\n';
		std::cerr << "\tdeviceType : " << devType << "\n";
		std::cerr << "\tdeviceName : " << devName << "\n";
		std::cerr << "\tpipelineCacheUUID : { ";
		for (uint32_t i = 0; i < VK_UUID_SIZE; ++i) {
			std::cerr << +deviceProperties.pipelineCacheUUID[i] << (i == VK_UUID_SIZE-1 ? " }\n" : ", ");
		}
	}

	return deviceProperties;
}

VkPhysicalDeviceFeatures Vulkan::DeviceFilter::getPhysicalDeviceFeatures(const VkPhysicalDevice& dev, bool printValues) {
	VkPhysicalDeviceFeatures devFeats = {};
	vkGetPhysicalDeviceFeatures(dev, &devFeats);

	if (printValues) {
		std::cerr << "Device features :\n";
		std::cerr << "\tHas robust buffer access ? ............... " << std::boolalpha << (bool)devFeats.robustBufferAccess << std::noboolalpha << "\n";
		std::cerr << "\tCan use full 32-bit range for indices ? .. " << (bool)devFeats.fullDrawIndexUint32 << "\n";
		std::cerr << "\tCan create image cube arrays ? ........... " << std::boolalpha << (bool)devFeats.imageCubeArray << std::noboolalpha << "\n";
		std::cerr << "\tSupports independant blending ? .......... " << std::boolalpha << (bool)devFeats.independentBlend << std::noboolalpha << "\n";
		std::cerr << "\tSupports geometry shaders ? .............. " << std::boolalpha << (bool)devFeats.geometryShader << std::noboolalpha << "\n";
		std::cerr << "\tSupports tesselation shaders ? ........... " << std::boolalpha << (bool)devFeats.tessellationShader << std::noboolalpha << "\n";
		std::cerr << "\tSupports Sample Shading ? ................ " << std::boolalpha << (bool)devFeats.sampleRateShading << std::noboolalpha << "\n";
		std::cerr << "\tCan blend two sources ? .................. " << std::boolalpha << (bool)devFeats.dualSrcBlend << std::noboolalpha << "\n";
		std::cerr << "\tSupports logic operations ? .............. " << std::boolalpha << (bool)devFeats.logicOp << std::noboolalpha << "\n";
		std::cerr << "\tCan use MultiDrawIndirect ? .............. " << std::boolalpha << (bool)devFeats.multiDrawIndirect << std::noboolalpha << "\n";
		std::cerr << "\tCan use wireframe/point modes ? .......... " << std::boolalpha << (bool)devFeats.fillModeNonSolid << std::noboolalpha << "\n";
		std::cerr << "\tSupports multiple viewports ? ............ " << std::boolalpha << (bool)devFeats.multiViewport << std::noboolalpha << "\n";
	}

	return devFeats;
}

Vulkan::DeviceFilter::QueueFamilies Vulkan::DeviceFilter::getAvailableQueues(const VkPhysicalDevice& dev, const VkSurfaceKHR& surface, bool printValues) {
	QueueFamilies families;

	std::vector<VkQueueFamilyProperties> queues = Vulkan::Enumerators::queueProperties(dev);

	if (printValues) {
		std::cerr << "Queue families :\n";
	}

	for (std::size_t i = 0; i < queues.size(); ++i) {
		if (queues[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) {
			families.graphicsFamily = i;
			if (printValues) {
				std::cerr << "\t[Queue " << i << "] Found " << queues[i].queueCount << " queues supporting graphics\n";
			}
		}
		if (queues[i].queueFlags & VK_QUEUE_COMPUTE_BIT) {
			families.computeFamily = i;
			if (printValues) {
				std::cerr << "\t[Queue " << i << "] Found " << queues[i].queueCount << " queues supporting compute\n";
			}
		}
		VkBool32 presentCapable = VK_FALSE;
		vkGetPhysicalDeviceSurfaceSupportKHR(dev, i, surface, &presentCapable);
		if (presentCapable == VK_TRUE) {
			families.presentFamily = i;
		}
	}

	return families;
}

Vulkan::DeviceFilter::SwapchainSupportDetails Vulkan::DeviceFilter::swapchainSupport(const VkPhysicalDevice& dev, const VkSurfaceKHR& surf, bool printValues) {
		SwapchainSupportDetails details;

		vkGetPhysicalDeviceSurfaceCapabilitiesKHR(dev, surf, &details.capabilities);
		details.formats = Vulkan::Enumerators::surfaceFormats(dev, surf);
		details.presentMode = Vulkan::Enumerators::presentModes(dev, surf);

		return details;
}

bool Vulkan::DeviceFilter::hasAllSurfaceFormats(const PhyDev& dev) {
	#warning Surface formats are not all checked. This only checks one is available
	for (const auto& f : dev.details.formats) {
		for (const auto& s : this->formats) {
			if (f.format == s.format && f.colorSpace == s.colorSpace) {
				return true;
			}
		}
	}
	return false;
}

bool Vulkan::DeviceFilter::hasAllPresentModes(const PhyDev& dev) {
	#warning Present modes are not all checked. This only checks one is available
	for (const auto& p : dev.details.presentMode) {
		for (const auto& s : this->presentMode) {
			if (p == s) {
				return true;
			}
		}
	}
	return false;
}

bool Vulkan::DeviceFilter::canFitWindowSize(const PhyDev& dev) {
	return dev.details.capabilities.currentExtent.width > this->winSize.width &&
		dev.details.capabilities.currentExtent.height > this->winSize.height;
}
