#include "vk_strings.hpp"

Vulkan::Version Vulkan::getVulkanVersion(uint32_t packed) {
	return {VK_VERSION_MAJOR(packed), VK_VERSION_MINOR(packed), VK_VERSION_PATCH(packed)};
}

std::string Vulkan::versionToString(Vulkan::Version v) {
	return std::string(std::to_string(v[0]) + '.' + std::to_string(v[1]) + '.' + std::to_string(v[2]));
}

using namespace Vulkan::Strings;

// VkResult values, matched with an error string {{{
std::vector<errors> Vulkan::Strings::errorCodes = {
	{ VK_NOT_READY , "Not ready" }, { VK_TIMEOUT, "Timeout" },
	{ VK_EVENT_SET , "Event set" }, { VK_EVENT_RESET , "Event reset" },
	{ VK_INCOMPLETE , "Incomplete" }, { VK_ERROR_OUT_OF_HOST_MEMORY , "Out of host memory" },
	{ VK_ERROR_OUT_OF_DEVICE_MEMORY , "Out of device memory" },
	{ VK_ERROR_INITIALIZATION_FAILED , "Device initialization failed" },
	{ VK_ERROR_DEVICE_LOST , "Device lost" },
	{ VK_ERROR_MEMORY_MAP_FAILED , "Memory mapping failed" },
	{ VK_ERROR_LAYER_NOT_PRESENT , "Layer not present" },
	{ VK_ERROR_EXTENSION_NOT_PRESENT , "Extension not present" },
	{ VK_ERROR_FEATURE_NOT_PRESENT , "Feature not present" },
	{ VK_ERROR_INCOMPATIBLE_DRIVER , "Incompatible driver" },
	{ VK_ERROR_TOO_MANY_OBJECTS , "Too many objects" },
	{ VK_ERROR_FORMAT_NOT_SUPPORTED , "Format not supported" },
	{ VK_ERROR_FRAGMENTED_POOL , "Fragmented pool" },
	{ VK_ERROR_UNKNOWN , "Unknown error" },
	{ VK_ERROR_OUT_OF_POOL_MEMORY , "Out of pooled memory" },
	{ VK_ERROR_INVALID_EXTERNAL_HANDLE , "Invalid external handle" },
	{ VK_ERROR_FRAGMENTATION , "Fragmentation" },
	{ VK_ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS , "Invalid opaque capture address" },
	{ VK_ERROR_SURFACE_LOST_KHR , "Surface lost" },
	{ VK_ERROR_NATIVE_WINDOW_IN_USE_KHR , "Native window in use" },
	{ VK_SUBOPTIMAL_KHR , "Suboptimal operation (from a KHR extension)" },
	{ VK_ERROR_OUT_OF_DATE_KHR , "Out-of-date software" },
	{ VK_ERROR_INCOMPATIBLE_DISPLAY_KHR , "Incompatible display" },
	{ VK_ERROR_VALIDATION_FAILED_EXT , "Validation failed" },
	{ VK_ERROR_INVALID_SHADER_NV , "Invalid shader" },
	//{ VK_ERROR_INCOMPATIBLE_VERSION_KHR , "Incompatible version" },
	{ VK_ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT , "Invalid DRM format modifier plane layout" },
	{ VK_ERROR_NOT_PERMITTED_EXT , "Not permitted" },
	{ VK_ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT , "Fullscreen exclusive mode lost" },
	{ VK_THREAD_IDLE_KHR , "Thread idle" }, { VK_THREAD_DONE_KHR , "Thread done" },
	{ VK_OPERATION_DEFERRED_KHR , "Operation defered" },
	{ VK_OPERATION_NOT_DEFERRED_KHR , "Operation not defered" },
	{ VK_PIPELINE_COMPILE_REQUIRED_EXT , "Pipeline compile required" },
};
// }}}

// getResultType() : Get a string for a given VkResult value {{{
std::string Vulkan::Strings::getResultType(VkResult _result) {
	/*
	if (VK_HEADER_VERSION_COMPLETE != VK_MAKE_VERSION(1,2,157)) {
		std::cerr << "WARNING : The error codes were created for Vulkan " <<
			" version 1.2.157, they might not be updated !\n";
	}
	*/
	if (_result >= VK_RESULT_MAX_ENUM) {
		std::cerr << "Result value out-of-bounds" << '\n';
		return "";
	}
	for (errors e : Vulkan::Strings::errorCodes) {
		if (e.result == _result) { return e.str; }
	}
	return "";
}
// }}}

// Message severities {{{
std::vector<severity> Vulkan::Strings::messageSeverities = {
	{ VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT , "Verbose" },
	{ VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT,     " Info. " },
	{ VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT,  "Warning" },
	{ VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT,    " Error " }
};
// }}}

std::string Vulkan::Strings::getMessageSeverity(VkDebugUtilsMessageSeverityFlagBitsEXT sev) {
	/*
	if (VK_HEADER_VERSION_COMPLETE != VK_MAKE_VERSION(1,2,157)) {
		std::cerr << "WARNING : The message severity types were created for Vulkan " <<
			" version 1.2.157, they might not be updated !\n";
	}
	*/
	if (sev >= VK_DEBUG_UTILS_MESSAGE_SEVERITY_FLAG_BITS_MAX_ENUM_EXT) {
		std::cerr << "Severity value out-of-bounds" << '\n';
		return "";
	}
	for (severity s : Vulkan::Strings::messageSeverities) {
		if (s.level == sev) { return s.str; }
	}
	return "";
}

std::vector<messagetypes> Vulkan::Strings::messageTypes = {
	{ VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT,     "General" },
	{ VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT,  "Valid. " },
	{ VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT, " Perf. " }
};

std::string Vulkan::Strings::getMessageType(VkDebugUtilsMessageTypeFlagsEXT t) {
	/*
	if (VK_HEADER_VERSION_COMPLETE != VK_MAKE_VERSION(1,2,157)) {
		std::cerr << "WARNING : The message types were created for Vulkan " <<
			" version 1.2.157, they might not be updated !\n";
	}
	*/
	if (t >= VK_DEBUG_UTILS_MESSAGE_TYPE_FLAG_BITS_MAX_ENUM_EXT) {
		std::cerr << "Message type value out-of-bounds" << '\n';
		return "";
	}
	for (messagetypes m : Vulkan::Strings::messageTypes) {
		if (m.type == t) { return m.str; }
	}
	return "";
}

std::vector<objects> Vulkan::Strings::objectTypes = {
	{ VK_OBJECT_TYPE_UNKNOWN , "Unknown"},
	{ VK_OBJECT_TYPE_INSTANCE , "Instance" },
	{ VK_OBJECT_TYPE_PHYSICAL_DEVICE , "Physical Device"},
	{ VK_OBJECT_TYPE_DEVICE , "Generic Device" },
	{ VK_OBJECT_TYPE_QUEUE , "Queue" },
	{ VK_OBJECT_TYPE_SEMAPHORE , "Semaphore" },
	{ VK_OBJECT_TYPE_COMMAND_BUFFER , "Command buffer" },
	{ VK_OBJECT_TYPE_FENCE , "Fence" },
	{ VK_OBJECT_TYPE_DEVICE_MEMORY , "Device Memory" },
	{ VK_OBJECT_TYPE_BUFFER , "Buffer" },
	{ VK_OBJECT_TYPE_IMAGE , "Image" },
	{ VK_OBJECT_TYPE_EVENT , "Event" },
	{ VK_OBJECT_TYPE_QUERY_POOL ,"Query pool" },
	{ VK_OBJECT_TYPE_BUFFER_VIEW , "Buffer view" },
	{ VK_OBJECT_TYPE_IMAGE_VIEW , "Image view" },
	{ VK_OBJECT_TYPE_SHADER_MODULE , "Shader module" },
	{ VK_OBJECT_TYPE_PIPELINE_CACHE , "Pipeline cache" },
	{ VK_OBJECT_TYPE_PIPELINE_LAYOUT , "Pipeline layout" },
	{ VK_OBJECT_TYPE_RENDER_PASS , "Render pass" },
	{ VK_OBJECT_TYPE_PIPELINE , "Pipeline" },
	{ VK_OBJECT_TYPE_DESCRIPTOR_SET_LAYOUT , "Descriptor set layout" },
	{ VK_OBJECT_TYPE_SAMPLER , "Sampler" },
	{ VK_OBJECT_TYPE_DESCRIPTOR_POOL , "Descriptor pool" },
	{ VK_OBJECT_TYPE_DESCRIPTOR_SET , "Descriptor set" },
	{ VK_OBJECT_TYPE_FRAMEBUFFER , "Framebuffer" },
	{ VK_OBJECT_TYPE_COMMAND_POOL , "Command pool" },
	{ VK_OBJECT_TYPE_SAMPLER_YCBCR_CONVERSION , "Sampler YCbCr conversion" },
	{ VK_OBJECT_TYPE_DESCRIPTOR_UPDATE_TEMPLATE , "Descriptor update template" },
	{ VK_OBJECT_TYPE_SURFACE_KHR , "Surface (KHR)" },
	{ VK_OBJECT_TYPE_SWAPCHAIN_KHR , "Swapchain (KHR)" },
	{ VK_OBJECT_TYPE_DISPLAY_KHR , "Display (KHR)" },
	{ VK_OBJECT_TYPE_DISPLAY_MODE_KHR , "Display mode (KHR)" },
	{ VK_OBJECT_TYPE_DEBUG_REPORT_CALLBACK_EXT , "Debug report callback (EXT)" },
	{ VK_OBJECT_TYPE_DEBUG_UTILS_MESSENGER_EXT , "Debug utils messenger (EXT)" },
	{ VK_OBJECT_TYPE_ACCELERATION_STRUCTURE_KHR , "Acceleration structure (KHR)" },
	{ VK_OBJECT_TYPE_VALIDATION_CACHE_EXT , "Validation cache (EXT)" },
	{ VK_OBJECT_TYPE_PERFORMANCE_CONFIGURATION_INTEL , "Performance configuration for Intel" },
	{ VK_OBJECT_TYPE_DEFERRED_OPERATION_KHR , "Deferred operation (KHR)" },
	{ VK_OBJECT_TYPE_INDIRECT_COMMANDS_LAYOUT_NV , "Indirect commands for Nvidia" },
	{ VK_OBJECT_TYPE_PRIVATE_DATA_SLOT_EXT , "Private data slot (EXT)" }
};

std::string Vulkan::Strings::getObjectType(VkObjectType objType) {
	/*
	if (VK_HEADER_VERSION_COMPLETE != VK_MAKE_VERSION(1,2,157)) {
		std::cerr << "WARNING : The object types were created for Vulkan " <<
			" version 1.2.157, they might not be updated !\n";
	}
	*/
	if (objType >= VK_OBJECT_TYPE_MAX_ENUM) {
		std::cerr << "Error : Object type value out-of-bounds (" << objType << ")\n";
		return "";
	}
	// Try to find the value, in the defined object types :
	for (objects o : Vulkan::Strings::objectTypes) {
		if (o.type == objType) { return o.str; }
	}
	return "";
}

std::vector<physicaldevicetype> Vulkan::Strings::physicalDeviceTypes = {
	{ VK_PHYSICAL_DEVICE_TYPE_OTHER , "Other" },
	{ VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU , "Integrated GPU" },
	{ VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU , "Discrete GPU" },
	{ VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU , "Virtual GPU" },
	{ VK_PHYSICAL_DEVICE_TYPE_CPU , "CPU" }
};

std::string Vulkan::Strings::getPhysicalDeviceType(VkPhysicalDeviceType t) {
	/*
	if (VK_HEADER_VERSION_COMPLETE != VK_MAKE_VERSION(1,2,157)) {
		std::cerr << "WARNING : The object types were created for Vulkan " <<
			" version 1.2.157, they might not be updated !\n";
	}
	*/
	if (t >= VK_PHYSICAL_DEVICE_TYPE_MAX_ENUM) {
		std::cerr << "Error : Physical device value out-of-bounds (" << t << ")\n";
		return "";
	}
	for (physicaldevicetype p : Vulkan::Strings::physicalDeviceTypes) {
		if (p.type == t) { return p.str; }
	}
	return "";
}

void Vulkan::Strings::printResultType(VkResult _result) {
	std::string resType = getResultType(_result);
	if (resType.empty()) {
		std::cerr << "Could not find the result type.\n";
	} else {
		std::cerr << "Result type : " << resType << '\n';
	}
}

void Vulkan::Strings::printMessageSeverity(VkDebugUtilsMessageSeverityFlagBitsEXT sev) {
	std::string msgSev = getMessageSeverity(sev);
	if (msgSev.empty()) {
		std::cerr << "Could not find the message severity level." << '\n';
	} else {
		std::cerr << "Message severity : " << msgSev << '\n';
	}
}

void Vulkan::Strings::printMessageType(VkDebugUtilsMessageTypeFlagsEXT t) {
	std::string msgType = getMessageType(t);
	if (msgType.empty()) {
		std::cerr << "Could not find the message type.\n";
	} else {
		std::cerr << "Message type : " << msgType << '\n';
	}
}

void Vulkan::Strings::printObjectType(VkObjectType objType) {
	std::string name = getObjectType(objType);
	if (name.empty()) {
		std::cerr << "Could not find the object type.\n";
	} else {
		std::cerr << "Object type : " << name << '\n';
	}
}

void Vulkan::Strings::printPhysicalDeviceType(VkPhysicalDeviceType t) {
	std::string name = getPhysicalDeviceType(t);
	if (name.empty()) {
		std::cerr << "Could not find the physical device type (" << t << ")\n";
	} else {
		std::cerr << "Physical device type : " << name << '\n';
	}
}






