#include "./basic_app.hpp"
#include "enumerators.hpp"
#include "vk_strings.hpp"
#include "vk.hpp"

#include "device_filter.hpp"
#include "helpers.hpp"

#include "vertex_specification.hpp"

#include <GLFW/glfw3.h>
#include <glm/gtc/matrix_transform.hpp>
#include <chrono>
#include <initializer_list>
#include <stdexcept>
#include <vulkan/vulkan_core.h>
#include <set>

#define STB_IMAGE_IMPLEMENTATION

#include <stb_image.h>

#define TINYOBJLOADER_IMPLEMENTATION

#include "./tiny_obj_loader.h"
#include <unordered_map>

#ifndef VK_DEBUG_MESSENGER_MANAGEMENT
#define VK_DEBUG_MESSENGER_MANAGEMENT

// Creation of a debug callback (the function should be loaded dynamically) {{{
VkResult CreateDebugUtilsMessengerEXT(VkInstance instance,
									  const VkDebugUtilsMessengerCreateInfoEXT *pCreateInfo,
									  const VkAllocationCallbacks *pAllocator,
									  VkDebugUtilsMessengerEXT *pDebugMessenger) {
	auto func = (PFN_vkCreateDebugUtilsMessengerEXT)
			vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT");

	if (func != nullptr) {
		return func(instance, pCreateInfo, pAllocator, pDebugMessenger);
	} else {
		return VK_ERROR_EXTENSION_NOT_PRESENT;
	}
}
// }}}

// Destruction of a debug callback (the function should be loaded dynamically) {{{
void DestroyDebugUtilsMessengerEXT(VkInstance instance,
								   VkDebugUtilsMessengerEXT debugMessenger,
								   const VkAllocationCallbacks *pAllocator) {
	auto func = (PFN_vkDestroyDebugUtilsMessengerEXT)
			vkGetInstanceProcAddr(instance, "vkDestroyDebugUtilsMessengerEXT");

	if (func != nullptr) {
		func(instance, debugMessenger, pAllocator);
	}
}
// }}}
#endif // VK_DEBUG_MESSENGER_MANAGEMENT

// Ctor {{{
BasicApp::BasicApp() {
	// Nothing here, yet.
	this->enabledDebug = false;
	this->requiredLayers = {};
	this->appWindow = nullptr;
	this->physicalDevice = VK_NULL_HANDLE;
	this->deviceExtensions = {VK_KHR_SWAPCHAIN_EXTENSION_NAME};
	this->fbResized = false;
	this->msaaSamples = VK_SAMPLE_COUNT_1_BIT;
}
// }}}

// Dtor {{{
BasicApp::~BasicApp() {
	this->waitIdle();
	this->cleanup();
}
// }}}

// Initialization of the app {{{
void BasicApp::init() {
	this->currentFrame = 0;
	this->requireLayers({"VK_LAYER_KHRONOS_validation"});
	this->enableDebugExt();
	this->createVulkanInstance();
	if (this->enabledDebug) {
		this->setupDebugMessenger();
	}
}

void BasicApp::setInitialSizes(int w, int h) {
	this->width = w;
	this->height = h;
	return;
}
// }}}

// Vulkan initialization {{{
void BasicApp::initVulkan() {
	this->pickPhysicalDevice();
	this->createLogicalDevice();
	this->getQueues();
	this->createSwapchain();
	this->createImageViews();
	this->createRenderPass();

	this->createDescriptorSetLayout();

	// Create the graphics pipeline (multiple sub-steps) :
	this->createGraphicsPipeline();

	this->createMSAARenderTarget();
	this->createDepthBuffer();
	this->createFramebuffers();
	this->createCommandPool();
	this->createTextureImage();
	this->createTextureView();
	this->createTextureSampler();
	this->loadModel();
	this->createVertexBuffer();
	this->createIndexBuffer();
	this->createUniformBuffers();
	this->createDescriptorPool();
	this->createDescriptorSets();
	this->createCommandBuffers();

	this->createSemaphores();
}
// }}}

// Init window {{{
void BasicApp::initWindow() {
}
// }}}

static void fbResizeCallback(GLFWwindow *win, int width, int height) {
	auto app = reinterpret_cast<BasicApp *>(glfwGetWindowUserPointer(win));
	app->fbResized = true;
}

// Draw function {{{
void BasicApp::drawFrame() {
	// wait for the frame to be rendered before drawing :
	vkWaitForFences(this->logicalDevice, 1, &this->flyingFences[this->currentFrame], VK_TRUE, UINT64_MAX);

	uint32_t imgIdx = 0;
	VkResult result = vkAcquireNextImageKHR(
		this->logicalDevice, this->swapchain, UINT64_MAX,
		this->imageAvailableSemaphores[this->currentFrame], VK_NULL_HANDLE,
		&imgIdx
	);

	if (result == VK_ERROR_OUT_OF_DATE_KHR) {
		this->reconstructSwapchain();
		return;
	} else if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR) {
		throw std::runtime_error("Cannot [re]create swapchain or acquire new images !");
	}

	// Checks if a previous frame uses this image :
	if (this->imageFences[imgIdx] != VK_NULL_HANDLE) {
		// wait for the image to be completely rendered :
		vkWaitForFences(this->logicalDevice, 1, &this->imageFences[imgIdx], VK_TRUE, UINT64_MAX);
	}

	// mark the image as in use :
	this->imageFences[imgIdx] = this->flyingFences[this->currentFrame];

	// update the uniform buffers with the right image :
	this->updateUniformBuffers(imgIdx);

	VkSemaphore sm[] = {this->imageAvailableSemaphores[this->currentFrame]};
	VkSemaphore siSm[] = {this->renderFinishedSemaphores[this->currentFrame]};
	VkPipelineStageFlags fl[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};

	VkSubmitInfo si{
		.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
		.pNext = nullptr,
		.waitSemaphoreCount = 1, .pWaitSemaphores = sm, .pWaitDstStageMask = fl,
		.commandBufferCount = 1, .pCommandBuffers = &this->commandBuffers[imgIdx],
		.signalSemaphoreCount = 1, .pSignalSemaphores = siSm
	};

	vkResetFences(this->logicalDevice, 1, &this->flyingFences[this->currentFrame]);

	if (vkQueueSubmit(this->graphicsQueue, 1, &si, this->flyingFences[this->currentFrame]) != VK_SUCCESS) {
		throw std::runtime_error("Cannot submit to queue !");
	}

	VkPresentInfoKHR present_info{
		.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
		.pNext = nullptr,
		.waitSemaphoreCount = 1, .pWaitSemaphores = siSm,
		.swapchainCount = 1, .pSwapchains = &this->swapchain,
		.pImageIndices = &imgIdx, .pResults = nullptr,
	};

	result = vkQueuePresentKHR(this->graphicsQueue, &present_info);

	if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR || this->fbResized) {
		this->fbResized = false;
		this->reconstructSwapchain();
		return;
	} else if (result != VK_SUCCESS) {
		throw std::runtime_error("Cannot [re]create swapchain or acquire new images !");
	}

	vkQueueWaitIdle(this->graphicsQueue);

	this->currentFrame = (this->currentFrame + 1) % MAX_FLIGHT;
}
// }}}

// Destroy the current swapchain {{{
void BasicApp::destroySwapchain() {
	for (const auto &fb: this->swapchainFramebuffers) {
		vkDestroyFramebuffer(this->logicalDevice, fb, nullptr);
	}

	for (std::size_t i = 0; i < this->swapchainImages.size(); ++i) {
		vkDestroyBuffer(this->logicalDevice, this->uniformBuffers[i], nullptr);
		vkFreeMemory(this->logicalDevice, this->uniformBuffersMemory[i], nullptr);
	}
	vkDestroyImageView(this->logicalDevice, this->colorImageView, nullptr);
	vkDestroyImage(this->logicalDevice, this->colorImage, nullptr);
	vkFreeMemory(this->logicalDevice, this->colorImageMemory, nullptr);
	vkDestroyImageView(this->logicalDevice, this->depthImageView, nullptr);
	vkDestroyImage(this->logicalDevice, this->depthImage, nullptr);
	vkFreeMemory(this->logicalDevice, this->depthImageMemory, nullptr);
	vkDestroyDescriptorPool(this->logicalDevice, this->descriptorPool, nullptr);
	vkFreeCommandBuffers(this->logicalDevice, this->commandPool,
						 static_cast<uint32_t>(this->commandBuffers.size()), this->commandBuffers.data());
	vkDestroyPipeline(this->logicalDevice, this->pipeline, nullptr);
	vkDestroyPipelineLayout(this->logicalDevice, this->pipelineLayout, nullptr);
	vkDestroyRenderPass(this->logicalDevice, this->renderPass, nullptr);
	for (auto &imageview: this->swapchainImageViews) {
		vkDestroyImageView(this->logicalDevice, imageview, nullptr);
	}
	vkDestroySwapchainKHR(this->logicalDevice, this->swapchain, nullptr);
}
// }}}

// Reconstructs the swapchain {{{
void BasicApp::resizeAll(int width, int height) {
	if (this->swapchainImages.size() == 0) {
		return;
	}
	this->fbResized = true;
	this->width = width;
	this->height = height;
}

void BasicApp::reconstructSwapchain() {
	// while (width == 0 || height == 0) {
	// 	glfwGetFramebufferSize(this->appWindow, &width, &height);
	// 	glfwWaitEvents();
	// }

	vkDeviceWaitIdle(this->logicalDevice);

	this->destroySwapchain();

	this->createSwapchain();
	this->createImageViews();
	this->createRenderPass();
	this->createGraphicsPipeline();
	this->createMSAARenderTarget();
	this->createDepthBuffer();
	this->createFramebuffers();
	this->createUniformBuffers();
	this->createDescriptorPool();
	this->createDescriptorSets();
	this->createCommandBuffers();
}
// }}}

// Cleanup of allocated resources {{{
void BasicApp::cleanup() {
	if (this->logicalDevice == VK_NULL_HANDLE) {
		return;
	}
	for (std::size_t i = 0; i < MAX_FLIGHT; ++i) {
		vkDestroySemaphore(this->logicalDevice, this->imageAvailableSemaphores[i], nullptr);
		vkDestroySemaphore(this->logicalDevice, this->renderFinishedSemaphores[i], nullptr);
		vkDestroyFence(this->logicalDevice, this->flyingFences[i], nullptr);
	}
	this->destroySwapchain();
	this->swapchainImages.clear();
	vkDestroySampler(this->logicalDevice, this->textureSampler, nullptr);
	vkDestroyImageView(this->logicalDevice, this->textureImageView, nullptr);
	vkDestroyImage(this->logicalDevice, this->textureImage, nullptr);
	vkFreeMemory(this->logicalDevice, this->textureImageMemory, nullptr);
	vkDestroyDescriptorSetLayout(this->logicalDevice, this->descriptorSetLayout, nullptr);
	vkDestroyBuffer(this->logicalDevice, this->indexBuffer, nullptr);
	vkFreeMemory(this->logicalDevice, this->indexBufferMemory, nullptr);
	vkDestroyBuffer(this->logicalDevice, this->vertexBuffer, nullptr);
	vkFreeMemory(this->logicalDevice, this->vertexBufferMemory, nullptr);
	vkDestroyCommandPool(this->logicalDevice, this->commandPool, nullptr);
	vkDestroyDevice(this->logicalDevice, nullptr);
	this->logicalDevice = VK_NULL_HANDLE;
	if (this->enabledDebug) {
		DestroyDebugUtilsMessengerEXT(this->instance, this->debugMessenger, nullptr);
	}
	//vkDestroySurfaceKHR(this->instance, this->surface, nullptr);
	//this->surface = VK_NULL_HANDLE;
	//vkDestroyInstance(this->instance, nullptr);
	//this->instance = VK_NULL_HANDLE;

	//glfwDestroyWindow(this->appWindow);
	//glfwTerminate();
}

void BasicApp::waitIdle() {
	if (this->logicalDevice != VK_NULL_HANDLE)
		vkDeviceWaitIdle(this->logicalDevice);
}
// }}}

// Require layer N {{{
BasicApp &BasicApp::requireLayers(std::initializer_list<const char *> layers) {
	for (const char *pLayerName: layers) {
		this->requiredLayers.push_back(pLayerName);
	}
	return *this;
}
// }}}

// Enable debug extension {{{
BasicApp &BasicApp::enableDebugExt(bool enabled) {
	this->enabledDebug = enabled;
	return *this;
}
// }}}

// Pick physical device, and create logical device {{{
void BasicApp::pickPhysicalDevice() {
	// Get all physical devices available for this instance :
	std::vector<VkPhysicalDevice> physicalDevices = Vulkan::Enumerators::physicalDevices(this->instance);
	// Filter them :
	this->devFilter = std::make_shared<Vulkan::DeviceFilter>(physicalDevices, this->surface);
	this->devFilter->addDeviceExtensions(this->deviceExtensions);

	VkSurfaceFormatKHR formatRequired{};
	formatRequired.format = VK_FORMAT_B8G8R8A8_SRGB;
	formatRequired.colorSpace = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR;
	this->devFilter->addSurfaceFormats({formatRequired});
	this->devFilter->addPresentModes({VK_PRESENT_MODE_FIFO_KHR});

	this->phyDev = std::make_shared<Vulkan::DeviceFilter::PhyDev>(
			this->devFilter->getBestDevice(this->deviceExtensions));
	this->physicalDevice = *this->phyDev->device;
	// update the max MSAA samples available :
	this->msaaSamples = this->getMaxUsableSampleCounts();
}

VkSampleCountFlagBits BasicApp::getMaxUsableSampleCounts() {
	VkPhysicalDeviceProperties _physical_device_properties;
	vkGetPhysicalDeviceProperties(this->physicalDevice, &_physical_device_properties);

	VkSampleCountFlags _flag_counts = _physical_device_properties.limits.framebufferColorSampleCounts &
									  _physical_device_properties.limits.framebufferDepthSampleCounts;

	if (_flag_counts & VK_SAMPLE_COUNT_64_BIT) {
		std::cerr << "Sample count max usable on the device : " << 64 << " samples.\n";
		return VK_SAMPLE_COUNT_64_BIT;
	}
	if (_flag_counts & VK_SAMPLE_COUNT_32_BIT) {
		std::cerr << "Sample count max usable on the device : " << 32 << " samples.\n";
		return VK_SAMPLE_COUNT_32_BIT;
	}
	if (_flag_counts & VK_SAMPLE_COUNT_16_BIT) {
		std::cerr << "Sample count max usable on the device : " << 16 << " samples.\n";
		return VK_SAMPLE_COUNT_16_BIT;
	}
	if (_flag_counts & VK_SAMPLE_COUNT_8_BIT) {
		std::cerr << "Sample count max usable on the device : " << 8 << " samples.\n";
		return VK_SAMPLE_COUNT_8_BIT;
	}
	if (_flag_counts & VK_SAMPLE_COUNT_4_BIT) {
		std::cerr << "Sample count max usable on the device : " << 4 << " samples.\n";
		return VK_SAMPLE_COUNT_4_BIT;
	}
	if (_flag_counts & VK_SAMPLE_COUNT_2_BIT) {
		std::cerr << "Sample count max usable on the device : " << 2 << " samples.\n";
		return VK_SAMPLE_COUNT_2_BIT;
	}
	// otherwise, only 1 sample can be used :
	std::cerr << "Sample count max usable on the device : " << 1 << " samples.\n";
	return VK_SAMPLE_COUNT_1_BIT;
}

void BasicApp::createLogicalDevice() {
	std::vector<VkDeviceQueueCreateInfo> queues;
	std::set<uint32_t> uniqueQueueIndices = {this->phyDev->queues.graphicsFamily.value(),
											 this->phyDev->queues.presentFamily.value()};
	float q = 1.f;

	for (uint32_t queueFamilies: uniqueQueueIndices) {
		VkDeviceQueueCreateInfo currentQueue{};
		currentQueue.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		currentQueue.queueFamilyIndex = queueFamilies;
		currentQueue.queueCount = 1;
		currentQueue.pQueuePriorities = &q;
		currentQueue.flags = 0;
		currentQueue.pNext = nullptr;
		queues.push_back(currentQueue);
	}

	VkDeviceCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	createInfo.pQueueCreateInfos = queues.data();
	createInfo.queueCreateInfoCount = queues.size();
	createInfo.pEnabledFeatures = &this->phyDev->feats;
	createInfo.enabledExtensionCount = this->deviceExtensions.size();
	createInfo.ppEnabledExtensionNames = this->deviceExtensions.data();
	createInfo.enabledLayerCount = 0;

	if (vkCreateDevice(this->physicalDevice, &createInfo, nullptr, &this->logicalDevice) != VK_SUCCESS) {
		throw std::runtime_error("Cannot create logical device !\n");
	}
}
// }}}

// Get device queues {{{
void BasicApp::getQueues() {
	vkGetDeviceQueue(this->logicalDevice, this->phyDev->queues.graphicsFamily.value(), 0, &this->graphicsQueue);
	vkGetDeviceQueue(this->logicalDevice, this->phyDev->queues.presentFamily.value(), 0, &this->presentQueue);
}
// }}}

// surface creation {{{
void BasicApp::setSurface(VkSurfaceKHR surface) {
	if (surface == 0) {
		throw std::runtime_error("the given surface was null!");
	}
	this->surface = surface;
}
// }}}

// Swapchain creation {{{
void BasicApp::createSwapchain() {
	// Gather details of the physical/logical device's surface capabilities :
	this->phyDev->details = this->devFilter->swapchainSupport(this->physicalDevice, this->surface);
	Vulkan::DeviceFilter::SwapchainSupportDetails details = this->phyDev->details;
	// To update each time. We need to have the current physical properties, since they describe the min/max extent of
	// the windows we can create and show.

	// Get the most appropriate surface format, present mode and extent (img size) :
	VkSurfaceFormatKHR sf = this->chooseSwapchainSupport(*this->phyDev);
	VkPresentModeKHR pm = this->chooseSwapchainPresentMode(*this->phyDev);
	VkExtent2D d2 = this->chooseSwapchainExtent(*this->phyDev);

	// std::min() here if minImages >= maxImages, otherwise take one more than minImages :
	uint32_t imgCount = std::min(details.capabilities.minImageCount + 1, details.capabilities.maxImageCount);
	// maxImages can be 0 if no limit is applied, so we don't want to query 0 images in that case :
	if (details.capabilities.maxImageCount == 0) {
		imgCount = details.capabilities.minImageCount + 1;
	}

	const Vulkan::DeviceFilter::QueueFamilies &fams = this->phyDev->queues;
	uint32_t queues[] = {fams.graphicsFamily.value(), fams.presentFamily.value()};

	VkSwapchainCreateInfoKHR createInfo = {
		.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
		.pNext = nullptr,
		//.flags = VK_SWAPCHAIN_CREATE_PROTECTED_BIT_KHR,
		.surface = this->surface,
		.minImageCount = imgCount,
		.imageFormat = sf.format,
		.imageColorSpace = sf.colorSpace,
		.imageExtent = d2,
		.imageArrayLayers = 1, // We have only one colored layer.
		.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
		.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE, /* By default, only one queue in exclusive sharing mode. */
		.queueFamilyIndexCount = 1,
		.pQueueFamilyIndices = queues, /* This can change later (below) */
		.preTransform = details.capabilities.currentTransform,
		.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
		.presentMode = pm,
		.clipped = VK_TRUE,
		.oldSwapchain = VK_NULL_HANDLE
	};

	if (queues[0] != queues[1]) {
		createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
		createInfo.queueFamilyIndexCount = 2;
		createInfo.pQueueFamilyIndices = queues;
	}

	auto created_swapchain = vkCreateSwapchainKHR(this->logicalDevice, &createInfo, nullptr, &this->swapchain);
	if (created_swapchain != VK_SUCCESS) {
		std::cerr << "vkCreateSwapchainKHR() returned " << created_swapchain << '\n';
		throw std::runtime_error("Could not create swapchain.");
	}

	// Gather images from the swapchain :
	uint32_t count = 0;
	vkGetSwapchainImagesKHR(this->logicalDevice, this->swapchain, &count, nullptr);
	this->swapchainImages.resize(count);
	vkGetSwapchainImagesKHR(this->logicalDevice, this->swapchain, &count, this->swapchainImages.data());

	// Save format and extent of the current swapchain :
	this->swapchainImageFormat = sf.format;
	this->swapchainExtent = d2;
}
// }}}

// Create Image views (right before pipeline creation) {{{
void BasicApp::createImageViews() {
	this->swapchainImageViews.resize(this->swapchainImages.size());

	for (std::size_t i = 0; i < this->swapchainImages.size(); ++i) {
		this->swapchainImageViews[i] = this->createImageView(this->swapchainImages[i], swapchainImageFormat,
															 VK_IMAGE_ASPECT_COLOR_BIT, 1);
	}
}
// }}}

// Create a render pass {{{
void BasicApp::createRenderPass() {
	//
	// Generate color attachment and attachment reference :
	//
	VkAttachmentDescription attachment = {};
	attachment.format = this->swapchainImageFormat;
	attachment.samples = this->msaaSamples;
	attachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	attachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	attachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	attachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	attachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	// MSAA images cannot be presented directly, so target is not PRESENT_SRC, but COLOR_ATTACHMENT_OPTIMAL:
	attachment.finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
	VkAttachmentReference ref = {};
	// Is 0 here because we have set:
	// "layout(location=0) out vec4 color;"
	// in the shader output !
	ref.attachment = 0;
	ref.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	//
	// Generate attachment & reference for the presentable target :
	//
	VkAttachmentDescription _color_attachment_resolve{};
	_color_attachment_resolve.format = this->swapchainImageFormat;
	_color_attachment_resolve.samples = VK_SAMPLE_COUNT_1_BIT;
	_color_attachment_resolve.loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	_color_attachment_resolve.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	_color_attachment_resolve.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	_color_attachment_resolve.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	_color_attachment_resolve.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	_color_attachment_resolve.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
	VkAttachmentReference _color_reference_resolve{};
	_color_reference_resolve.attachment = 2;
	_color_reference_resolve.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	//
	// Generate depth attachment and attachment reference :
	//
	VkAttachmentDescription _depth_attachment{};
	_depth_attachment.format = this->findDepthFormat();
	_depth_attachment.samples = this->msaaSamples;
	_depth_attachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	_depth_attachment.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE; // won't be used after drawing, don't care.
	_depth_attachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	_depth_attachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	_depth_attachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED; // don't care about previous state
	_depth_attachment.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
	VkAttachmentReference _depth_reference{};
	_depth_reference.attachment = 1;
	_depth_reference.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

	VkSubpassDescription subdesc{};
	subdesc.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	subdesc.colorAttachmentCount = 1;
	subdesc.pColorAttachments = &ref;
	subdesc.pResolveAttachments = &_color_reference_resolve;
	subdesc.pDepthStencilAttachment = &_depth_reference;

	VkSubpassDependency spd{};
	spd.srcSubpass = VK_SUBPASS_EXTERNAL;
	spd.dstSubpass = 0;
	spd.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	spd.srcAccessMask = 0;
	spd.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	spd.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

	const std::array<VkAttachmentDescription, 3> _all_attachment_references = {attachment, _depth_attachment,
																			   _color_attachment_resolve};

	VkRenderPassCreateInfo rci = {};
	rci.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	rci.attachmentCount = static_cast<uint32_t>(_all_attachment_references.size());
	rci.pAttachments = _all_attachment_references.data();
	rci.subpassCount = 1;
	rci.pSubpasses = &subdesc;
	rci.dependencyCount = 1;
	rci.pDependencies = &spd;

	if (vkCreateRenderPass(this->logicalDevice, &rci, nullptr, &this->renderPass) != VK_SUCCESS) {
		throw std::runtime_error("Could not create render pass.");
	}
}
// }}}

// Create the descriptor sets {{{
void BasicApp::createDescriptorSetLayout() {
	//
	// CREATE UBO LAYOUT :
	//
	VkDescriptorSetLayoutBinding _ubo_layout_binding{};
	_ubo_layout_binding.binding = 0; // same value as in the shader !
	_ubo_layout_binding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	_ubo_layout_binding.descriptorCount = 1;
	// note : even though we'll pass 3 matrices, there is only one struct bound on the
	// shader side. hence the "1" here. if an array of UBOs was bound, there would be
	// more than 1 here.
	_ubo_layout_binding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT; // we want to bind it in the VShader !
	_ubo_layout_binding.pImmutableSamplers = nullptr; //used for image sampling, thus none here

	//
	// CREATE SAMPLER LAYOUT :
	//
	VkDescriptorSetLayoutBinding _sampler_layout_binding{};
	_sampler_layout_binding.binding = 1;
	_sampler_layout_binding.descriptorCount = 1;
	_sampler_layout_binding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	_sampler_layout_binding.pImmutableSamplers = nullptr; // only used for sampler that _will not_ change !!!
	_sampler_layout_binding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT; //used in the fshader !

	//
	// CREATE LAYOUT INFO :
	//
	std::array<VkDescriptorSetLayoutBinding, 2> _bindings = {_ubo_layout_binding, _sampler_layout_binding};
	VkDescriptorSetLayoutCreateInfo _layout_info{};
	_layout_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	_layout_info.bindingCount = static_cast<uint32_t>(_bindings.size());
	_layout_info.pBindings = _bindings.data();

	//
	// CREATE DESCRIPTOR SET LAYOUT OBJECT :
	//
	if (vkCreateDescriptorSetLayout(this->logicalDevice, &_layout_info, nullptr, &this->descriptorSetLayout) !=
		VK_SUCCESS) {
		throw std::runtime_error("could not create the descriptor set layout !!!");
	}
}
// }}}

// Create a descriptor pool {{{
void BasicApp::createDescriptorPool() {
	std::array<VkDescriptorPoolSize, 2> _pool_sizes{};
	// UBO (3 mat4s) descriptor :
	_pool_sizes[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	_pool_sizes[0].descriptorCount = static_cast<uint32_t>(this->swapchainImages.size());
	// we need one descriptor set for each frame, since that's the number of uniform buffer we have
	// Combined image descriptor :
	_pool_sizes[1].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	_pool_sizes[1].descriptorCount = static_cast<uint32_t>(this->swapchainImages.size());

	VkDescriptorPoolCreateInfo _pool_create_info{};
	_pool_create_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	_pool_create_info.poolSizeCount = static_cast<uint32_t>(_pool_sizes.size());
	_pool_create_info.pPoolSizes = _pool_sizes.data();
	_pool_create_info.maxSets = static_cast<uint32_t>(this->swapchainImages.size()); // same rationale

	if (vkCreateDescriptorPool(this->logicalDevice, &_pool_create_info, nullptr, &this->descriptorPool) != VK_SUCCESS) {
		throw std::runtime_error("Cannot create descriptor pool !");
	}
}
// }}}

// Create the descriptor sets {{{
void BasicApp::createDescriptorSets() {
	std::vector<VkDescriptorSetLayout> layouts(this->swapchainImages.size(), this->descriptorSetLayout);

	if (this->swapchainImages.empty()) { std::cerr << "Error : creating 0 descriptor sets\n"; }

	VkDescriptorSetAllocateInfo _set_alloc_info{};
	_set_alloc_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	_set_alloc_info.descriptorPool = this->descriptorPool;
	_set_alloc_info.descriptorSetCount = static_cast<uint32_t>(this->swapchainImages.size());
	_set_alloc_info.pSetLayouts = layouts.data();

	this->descriptorSets.resize(_set_alloc_info.descriptorSetCount);
	if (vkAllocateDescriptorSets(this->logicalDevice, &_set_alloc_info, this->descriptorSets.data()) != VK_SUCCESS) {
		throw std::runtime_error("failed to allocate descriptor sets !!!");
	}

	for (std::size_t i = 0; i < _set_alloc_info.descriptorSetCount; ++i) {
		VkDescriptorBufferInfo _buf_info{};
		_buf_info.buffer = this->uniformBuffers[i];
		_buf_info.offset = 0;
		_buf_info.range = sizeof(UniformBufferObject);

		VkDescriptorImageInfo _img_info{};
		_img_info.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		_img_info.imageView = this->textureImageView;
		_img_info.sampler = this->textureSampler;

		std::array<VkWriteDescriptorSet, 2> _desc_write{};
		_desc_write[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		_desc_write[0].dstSet = this->descriptorSets[i];
		_desc_write[0].dstBinding = 0; // from the shader !
		_desc_write[0].dstArrayElement = 0;
		_desc_write[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		_desc_write[0].descriptorCount = 1;
		_desc_write[0].pBufferInfo = &_buf_info;
		_desc_write[0].pImageInfo = nullptr; // for image views, optional
		_desc_write[0].pTexelBufferView = nullptr; // for buffer views, optional

		_desc_write[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		_desc_write[1].dstSet = this->descriptorSets[i];
		_desc_write[1].dstBinding = 1; // from the shader !
		_desc_write[1].dstArrayElement = 0;
		_desc_write[1].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		_desc_write[1].descriptorCount = 1;
		_desc_write[1].pBufferInfo = nullptr;
		_desc_write[1].pImageInfo = &_img_info;
		_desc_write[1].pTexelBufferView = nullptr;

		// update the info and upload data !
		vkUpdateDescriptorSets(this->logicalDevice, static_cast<uint32_t>(_desc_write.size()), _desc_write.data(), 0,
							   nullptr);
	}
}
// }}}

// Graphics pipeline creation {{{
void BasicApp::createGraphicsPipeline() {
	// Read the shader's bytcode :
	auto vertShaderCode = readFile("../../shaders/vert.spv");
	auto fragShaderCode = readFile("../../shaders/frag.spv");

	auto bindingDescription = Vertex::getBindingDescription();
	auto attributeDescription = Vertex::getAttributeDescription();

	// Create vulkan wrappers around the bytecode :
	VkShaderModule vertShader = this->createShaderModule(vertShaderCode);
	VkShaderModule fragShader = this->createShaderModule(fragShaderCode);

	// Creation information for vertex shader :
	VkPipelineShaderStageCreateInfo vertShaderCreateInfo = {};
	vertShaderCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	vertShaderCreateInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;
	vertShaderCreateInfo.module = vertShader;
	vertShaderCreateInfo.pName = "main"; // Specifies the entrypoint, can be changed !!! :D
	// Same for fragment shader :
	VkPipelineShaderStageCreateInfo fragShaderCreateInfo = {};
	fragShaderCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	fragShaderCreateInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
	fragShaderCreateInfo.module = fragShader;
	fragShaderCreateInfo.pName = "main"; // Specifies the entrypoint, can be changed !!! :D
	// Regroup them in a struct :
	VkPipelineShaderStageCreateInfo shaderStages[] = {vertShaderCreateInfo, fragShaderCreateInfo};

	// Create input vertex shader info : (what will be passed to it)
	VkPipelineVertexInputStateCreateInfo vertInputInfo = {};
	vertInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
	vertInputInfo.vertexBindingDescriptionCount = 1;                    // Pass the data that the Vertex struct generates
	vertInputInfo.pVertexBindingDescriptions = &bindingDescription;        // in those parameters !
	vertInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(attributeDescription.size());
	vertInputInfo.pVertexAttributeDescriptions = attributeDescription.data();

	// Specify the input assembly functions :
	VkPipelineInputAssemblyStateCreateInfo inputAssemblyInfo = {};
	inputAssemblyInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	inputAssemblyInfo.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	inputAssemblyInfo.primitiveRestartEnable = VK_FALSE;

	// Specify the viewport parameters :
	VkViewport viewport = {};
	viewport.x = .0f;
	viewport.y = .0f;
	viewport.width = (float) this->swapchainExtent.width;
	viewport.height = (float) this->swapchainExtent.height;
	viewport.minDepth = .0f;
	viewport.maxDepth = 1.f;
	// Specify scissor rectangle :
	VkRect2D scissor{{0, 0}, this->swapchainExtent};

	// Create the pipeline viewport :
	VkPipelineViewportStateCreateInfo viewportState = {};
	viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	viewportState.viewportCount = 1;
	viewportState.pViewports = &viewport;
	viewportState.scissorCount = 1;
	viewportState.pScissors = &scissor;

	VkPipelineDepthStencilStateCreateInfo _depth_stencil_create_info{};
	_depth_stencil_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
	_depth_stencil_create_info.depthTestEnable = VK_TRUE;    // Enable depth testing, but doesn't write the new values
	_depth_stencil_create_info.depthWriteEnable = VK_TRUE;    // Enable depth writes after a fragment's valid depth pass
	_depth_stencil_create_info.depthCompareOp = VK_COMPARE_OP_LESS; // Want to obscure by strictly less than present
	_depth_stencil_create_info.depthBoundsTestEnable = VK_FALSE; // don't clip against predefined bounds, default's fine
	_depth_stencil_create_info.minDepthBounds = 0.0f; // optional, 0.0 is default
	_depth_stencil_create_info.maxDepthBounds = 1.0f; // optional, 1.0 is default
	_depth_stencil_create_info.stencilTestEnable = VK_FALSE; // don't use stencil right now
	_depth_stencil_create_info.front = {};
	_depth_stencil_create_info.back = {};

	// Specify rasterizer parameters :
	VkPipelineRasterizationStateCreateInfo rasterizer = {};
	rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	rasterizer.depthClampEnable = VK_FALSE;            // If true, clamps far/near fragments instead of discarding them ...
	rasterizer.rasterizerDiscardEnable = VK_FALSE;    // Disables _ANY_ output to the viewport/framebuffer (!!!)
	rasterizer.polygonMode = VK_POLYGON_MODE_FILL;    // Equivalent to the old glPolygonMode();
	rasterizer.lineWidth = 1.f;                        // Equivalent to the old glLineWidth();
	rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;    // Cull back polygons
	rasterizer.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;    // The direction of the 'front' face.
	rasterizer.depthBiasEnable = VK_FALSE;            // Depth bias alters _all_ fragments by adding a constant to them, or biasing them based on slope.

	// Pipeline multisampling creation info :
	VkPipelineMultisampleStateCreateInfo multisampling = {};
	multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	multisampling.sampleShadingEnable = VK_TRUE;
	multisampling.minSampleShading = .2f;
	multisampling.rasterizationSamples = this->msaaSamples;
	// for now, multisampling is disabled.

	// We can also create a VkPipelineDepthStencilStateCreateInfo struct if we have a depth/stencil buffer.

	// Color blending settings :
	// (this particular struct is done per-framebuffer)
	VkPipelineColorBlendAttachmentState colorBlendAttachment = {};
	colorBlendAttachment.colorWriteMask =
			VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
	colorBlendAttachment.blendEnable = VK_FALSE;
	colorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;
	colorBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
	colorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD;
	colorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
	colorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
	colorBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD;
	// this is for the final color blending stage (regardless of framebuffer requirements) :
	VkPipelineColorBlendStateCreateInfo colorBlending = {};
	colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	colorBlending.logicOpEnable = VK_FALSE;
	colorBlending.attachmentCount = 1;
	colorBlending.pAttachments = &colorBlendAttachment;
	colorBlending.blendConstants[0] = .0f;
	colorBlending.blendConstants[1] = .0f;
	colorBlending.blendConstants[2] = .0f;
	colorBlending.blendConstants[3] = .0f;

	// Some parameters can be changed without having to recreate the entire pipeline
	// object. Those are line widths, framebuffer extent (viewport size) and blend
	// constants. If those are to be changed, we need to fill in a struct as such :
	// VkDynamicState states[] = { ... };
	// VkPipelineDynamicStateCreateInfo state = {};
	// fill in data and add to pipeline creation

	// Pipeline layout (specifies the uniforms used) :
	VkPipelineLayoutCreateInfo pipeLayoutInfo = {};
	pipeLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	pipeLayoutInfo.setLayoutCount = 1; // The UBO is the only descriptor we'll have for now
	pipeLayoutInfo.pSetLayouts = &this->descriptorSetLayout;

	if (vkCreatePipelineLayout(this->logicalDevice, &pipeLayoutInfo, nullptr, &this->pipelineLayout) != VK_SUCCESS) {
		throw std::runtime_error("Cannot create pipeline layout !");
	}

	VkGraphicsPipelineCreateInfo pci = {};
	pci.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
	pci.pNext = nullptr;
	// pci.flags = VK_PIPELINE_CREATE_DISABLE_OPTIMIZATION_BIT;
	pci.stageCount = 2;
	pci.pStages = shaderStages;
	pci.pVertexInputState = &vertInputInfo;
	pci.pInputAssemblyState = &inputAssemblyInfo;
	pci.pTessellationState = nullptr;
	pci.pViewportState = &viewportState;
	pci.pRasterizationState = &rasterizer;
	pci.pMultisampleState = &multisampling;
	pci.pDepthStencilState = &_depth_stencil_create_info;    // Use depth testing !!
	pci.pColorBlendState = &colorBlending;
	pci.pDynamicState = nullptr;

	pci.layout = this->pipelineLayout;
	pci.renderPass = this->renderPass;
	pci.subpass = 0;
	pci.basePipelineHandle = VK_NULL_HANDLE;
	pci.basePipelineIndex = -1;

	if (vkCreateGraphicsPipelines(this->logicalDevice, VK_NULL_HANDLE, 1, &pci, nullptr, &this->pipeline) !=
		VK_SUCCESS) {
		throw std::runtime_error("Cannot create graphics pipeline");
	}

	vkDestroyShaderModule(this->logicalDevice, fragShader, nullptr);
	vkDestroyShaderModule(this->logicalDevice, vertShader, nullptr);
}
// }}}

// Create framebuffers {{{
void BasicApp::createFramebuffers() {
	this->swapchainFramebuffers.resize(this->swapchainImageViews.size());

	for (std::size_t i = 0; i < this->swapchainFramebuffers.size(); ++i) {
		// attach both color and depth/stencil buffers :
		std::array<VkImageView, 3> attachments = {
				this->colorImageView,
				this->depthImageView, // can have only 1, semaphores guarantee no 2 frames are computed at the same time
				this->swapchainImageViews[i],
		};

		VkFramebufferCreateInfo fbi = {};
		fbi.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
		fbi.renderPass = this->renderPass;
		fbi.attachmentCount = static_cast<uint32_t>(attachments.size());
		fbi.pAttachments = attachments.data();
		fbi.width = this->swapchainExtent.width;
		fbi.height = this->swapchainExtent.height;
		fbi.layers = 1;

		if (vkCreateFramebuffer(this->logicalDevice, &fbi, nullptr, &this->swapchainFramebuffers[i]) != VK_SUCCESS) {
			throw std::runtime_error("Cannot create framebuffer");
		}
	}
}
// }}}

// Create a command pool {{{
void BasicApp::createCommandPool() {
	Vulkan::DeviceFilter::QueueFamilies queues = this->phyDev->queues;

	VkCommandPoolCreateInfo cpci{};
	cpci.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	cpci.queueFamilyIndex = queues.graphicsFamily.value();
	cpci.flags = 0;

	if (vkCreateCommandPool(this->logicalDevice, &cpci, nullptr, &this->commandPool) != VK_SUCCESS) {
		throw std::runtime_error("Cannot create command pool");
	}
}
// }}}

// Create, and record command buffers {{{
void BasicApp::createCommandBuffers() {
	this->commandBuffers.resize(this->swapchainFramebuffers.size());

	VkCommandBufferAllocateInfo cbai{
		.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
		.pNext = nullptr,
		.commandPool = this->commandPool, .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
		.commandBufferCount = static_cast<uint32_t>(this->commandBuffers.size())
	};

	if (vkAllocateCommandBuffers(this->logicalDevice, &cbai, this->commandBuffers.data()) != VK_SUCCESS) {
		throw std::runtime_error("Cannot create command buffers");
	}

	for (std::size_t i = 0; i < this->commandBuffers.size(); ++i) {
		VkCommandBufferBeginInfo cbbi{};
		cbbi.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		cbbi.flags = 0;
		cbbi.pInheritanceInfo = nullptr;

		if (vkBeginCommandBuffer(this->commandBuffers[i], &cbbi) != VK_SUCCESS) {
			throw std::runtime_error("Cannot begin buffer.");
		}

		//
		// Generate clearing values for color and depth/stencil buffers :
		//
		std::array<VkClearValue, 2> cv{}; // clear values for color and depth buffers
		cv[0] = {.0f, .0f, .0f, 1.f}; // clear for RGBA channels
		cv[1] = {1.0f, 0.0f}; // only depth and stencil, so 2 values
		// WARNING : clearing values should be in the same order as the buffer attachments in createRenderPass()

		VkRenderPassBeginInfo rpbi{
			.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
			.pNext = nullptr,
			.renderPass = this->renderPass, .framebuffer = this->swapchainFramebuffers[i],
			.renderArea = VkRect2D{.offset = {0, 0}, .extent = this->swapchainExtent},
			.clearValueCount = static_cast<uint32_t>(cv.size()), .pClearValues = cv.data()
		};

		// Begin render pass :
		vkCmdBeginRenderPass(this->commandBuffers[i], &rpbi, VK_SUBPASS_CONTENTS_INLINE);

		// Bind pipeline (akin to glBindProgram() in OpenGL)
		vkCmdBindPipeline(this->commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, this->pipeline);

		// BINDING THE VERTEX BUFFER :
		VkBuffer vBuffers[] = {this->vertexBuffer};
		VkDeviceSize offsets[] = {0};
		// bind to location 0, with 1 binding point and 0 bytes of offset
		vkCmdBindVertexBuffers(this->commandBuffers[i], 0, 1, vBuffers, offsets);
		// bind index buffer, with offset of 0 bytes into the allocated buffer and type uint16_t :
		vkCmdBindIndexBuffer(this->commandBuffers[i], this->indexBuffer, 0, VK_INDEX_TYPE_UINT32);
		// bind the descriptors (UBOs) :
		vkCmdBindDescriptorSets(this->commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS,
								this->pipelineLayout, 0, 1, &this->descriptorSets[i],
								0, nullptr);
		// draw using the index buffer data :
		vkCmdDrawIndexed(this->commandBuffers[i], static_cast<uint32_t>(this->vert_indices.size()), 1, 0, 0, 0);

		vkCmdEndRenderPass(this->commandBuffers[i]);

		if (vkEndCommandBuffer(this->commandBuffers[i]) != VK_SUCCESS) {
			throw std::runtime_error("Cannot end command buffer.");
		}
	}

}
// }}}

/// Generic function to create a buffer {{{
void BasicApp::createBuffer(VkDeviceSize _buffer_size, VkBufferUsageFlags _buffer_usage,
							VkMemoryPropertyFlags _buffer_memory_properties, VkBuffer &_buffer,
							VkDeviceMemory &_buffer_memory) {
	// create the buffer with the given properties :
	VkBufferCreateInfo _buffer_create_info{};
	_buffer_create_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	_buffer_create_info.size = _buffer_size;
	_buffer_create_info.usage = _buffer_usage;
	_buffer_create_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

	if (vkCreateBuffer(this->logicalDevice, &_buffer_create_info, nullptr, &_buffer) != VK_SUCCESS) {
		throw std::runtime_error("failed to create buffer in createBuffer() !");
	}

	// get memory requirements for the platform/device/buffer :
	VkMemoryRequirements _mem_requirements{};
	vkGetBufferMemoryRequirements(this->logicalDevice, _buffer, &_mem_requirements);

	// allocate memory :
	VkMemoryAllocateInfo _alloc_info{};
	_alloc_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	_alloc_info.allocationSize = _mem_requirements.size;
	_alloc_info.memoryTypeIndex = findMemoryType(_mem_requirements.memoryTypeBits, _buffer_memory_properties);

	if (vkAllocateMemory(this->logicalDevice, &_alloc_info, nullptr, &_buffer_memory) != VK_SUCCESS) {
		throw std::runtime_error("failed to allocate buffer memory in createBuffer() !");
	}

	// finally, bind buffer object and its memory pool
	vkBindBufferMemory(this->logicalDevice, _buffer, _buffer_memory, 0);
}
/// }}}

// Create vertex buffers {{{
void BasicApp::createVertexBuffer() {
	// Approximate vertex buffer size :
	VkDeviceSize _buffer_size = sizeof(this->vertices[0]) * this->vertices.size();

	// create the host-local vertex buffer, only used for staging the transfer :
	VkBuffer _staging_buffer;                // CPU-side buf for vertex data
	VkDeviceMemory _staging_buffer_memory;    // The mem storage of _staging_buffer
	this->createBuffer(_buffer_size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
					   VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
					   _staging_buffer, _staging_buffer_memory);

	//
	// MAP MEMORY FROM HOST :
	//
	void *data;
	// vkMapMemory allows access to a region of the memory resource defined by the offset & size :
	vkMapMemory(this->logicalDevice, _staging_buffer_memory, 0, _buffer_size, 0, &data);
	// copy data over to it :
	memcpy(data, this->vertices.data(), (size_t) _buffer_size);
	// unmap the memory buffer :
	vkUnmapMemory(this->logicalDevice, _staging_buffer_memory);
	// WARNING, data copy to device not always performed now. Could be delayed because of caching issues (...). To do
	// so immediately, have a memory region with the VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, or call
	// vkFlushMappedMemoryRanges() after writing.
	// N.B. : using host-coherent memory may hurt performance.

	// create the device-local vertex buffer :
	this->createBuffer(_buffer_size, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
					   VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
					   this->vertexBuffer, this->vertexBufferMemory);

	// copy data from staging to vertex buffer :
	this->copyBuffer(_staging_buffer, this->vertexBuffer, _buffer_size);

	// remove and delete staging buffer :
	vkDestroyBuffer(this->logicalDevice, _staging_buffer, nullptr);
	vkFreeMemory(this->logicalDevice, _staging_buffer_memory, nullptr);
}
// }}}

// Copy data from one buffer to another {{{
void BasicApp::copyBuffer(VkBuffer _src_buffer, VkBuffer _dst_buffer, VkDeviceSize _buffer_size) {
	// Transfer commands are queued to a command buffer, like draw commands.
	// Thus, we must create a local command buffer.

	VkCommandBuffer local_buffer = this->beginOneTimeCommands();
	{
		// Add the copy command to the buffer :
		VkBufferCopy _buf_copy_region{};
		_buf_copy_region.srcOffset = 0;
		_buf_copy_region.dstOffset = 0;
		_buf_copy_region.size = _buffer_size;
		vkCmdCopyBuffer(local_buffer, _src_buffer, _dst_buffer, 1, &_buf_copy_region);
	}
	this->endOneTimeCommands(local_buffer);
}
// }}}

// Create the index buffer {{{
void BasicApp::createIndexBuffer() {
	VkDeviceSize _buffer_size = sizeof(this->vert_indices[0]) * this->vert_indices.size();

	// create the host-local vertex buffer, only used for staging the transfer :
	VkBuffer _staging_buffer;                // CPU-side buf for vertex data
	VkDeviceMemory _staging_buffer_memory;    // The mem storage of _staging_buffer
	this->createBuffer(_buffer_size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
					   VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
					   _staging_buffer, _staging_buffer_memory);

	//
	// MAP MEMORY FROM HOST :
	//
	void *data;
	// vkMapMemory allows access to a region of the memory resource defined by the offset & size :
	vkMapMemory(this->logicalDevice, _staging_buffer_memory, 0, _buffer_size, 0, &data);
	// copy data over to it :
	memcpy(data, this->vert_indices.data(), (size_t) _buffer_size);
	// unmap the memory buffer :
	vkUnmapMemory(this->logicalDevice, _staging_buffer_memory);
	// WARNING, data copy to device not always performed now. Could be delayed because of caching issues (...). To do
	// so immediately, have a memory region with the VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, or call
	// vkFlushMappedMemoryRanges() after writing.
	// N.B. : using host-coherent memory may hurt performance.

	// create the device-local vertex buffer :
	this->createBuffer(_buffer_size, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
					   VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
					   this->indexBuffer, this->indexBufferMemory);

	// copy data from staging to vertex buffer :
	this->copyBuffer(_staging_buffer, this->indexBuffer, _buffer_size);

	// remove and delete staging buffer :
	vkDestroyBuffer(this->logicalDevice, _staging_buffer, nullptr);
	vkFreeMemory(this->logicalDevice, _staging_buffer_memory, nullptr);
}
// }}}

// Create uniform buffers {{{
void BasicApp::createUniformBuffers() {
	VkDeviceSize _ubo_size = sizeof(UniformBufferObject);

	this->uniformBuffers.resize(this->swapchainImages.size());
	this->uniformBuffersMemory.resize(this->swapchainImages.size());

	for (std::size_t i = 0; i < this->swapchainImages.size(); ++i) {
		this->createBuffer(_ubo_size, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
						   VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
						   this->uniformBuffers[i], this->uniformBuffersMemory[i]);
	}
}
// }}}

// Load a model {{{
void BasicApp::loadModel() {
	tinyobj::attrib_t attrib;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;
	std::string warn, err;

	if (!tinyobj::LoadObj(&attrib, &shapes, &materials, &warn, &err, MODEL_PATH)) {
		throw std::runtime_error("cannot load model !!!" + warn + err);
	}

	std::unordered_map<Vertex, uint32_t> vertex_map{};

	for (const auto &shape: shapes) {
		for (const auto &index: shape.mesh.indices) {
			Vertex v{};
			v.pos = {
					attrib.vertices[3 * index.vertex_index + 0],
					attrib.vertices[3 * index.vertex_index + 1],
					attrib.vertices[3 * index.vertex_index + 2]
			};
			v.texCoord = {
					attrib.texcoords[2 * index.texcoord_index + 0],
					1.f - attrib.texcoords[2 * index.texcoord_index + 1]
			};
			v.color = {1.f, 1.f, 1.f};

			if (vertex_map.count(v) == 0) {
				vertex_map[v] = static_cast<uint32_t>(this->vertices.size());
				this->vertices.push_back(v);
			}
			this->vert_indices.push_back(vertex_map[v]);

			//this->vertices.push_back(v);
			//this->vert_indices.push_back(this->vert_indices.size());
		}
	}
}
// }}}

// Update the uniform buffers {{{
void BasicApp::updateUniformBuffers(uint32_t image_in_swapchain) {
	static auto start_time = std::chrono::high_resolution_clock::now();

	// get time elapsed since the beginning :
	auto current_time = std::chrono::high_resolution_clock::now();
	float time_elapsed = std::chrono::duration<float, std::chrono::seconds::period>(current_time - start_time).count();

	UBO ubo{};
	ubo.model = glm::rotate(glm::mat4(1.f), time_elapsed * glm::radians(90.f), glm::vec3(0.f, 0.f, 1.f));
	ubo.view = glm::lookAt(glm::vec3(2.f, 2.f, 2.f), glm::vec3(0.f, 0.f, 0.f), glm::vec3(0.f, 0.f, 1.f));
	ubo.proj = glm::perspective(glm::radians(25.f), ((float) swapchainExtent.width) / (float) swapchainExtent.height,
								0.1f, 10.f);

	ubo.proj[1][1] *= -1.f; // GLM was for OpenGL, need to invert the Y axis to conform to Vulkan's coordinate system

	// Copy data over to the buffer :
	void *data;
	vkMapMemory(this->logicalDevice, this->uniformBuffersMemory[image_in_swapchain], 0, sizeof(ubo), 0, &data);
	memcpy(data, &ubo, sizeof(ubo));
	vkUnmapMemory(this->logicalDevice, this->uniformBuffersMemory[image_in_swapchain]);
}
// }}}

/// Find a suitable memory type based on properties and filters {{{
uint32_t BasicApp::findMemoryType(uint32_t filter, VkMemoryPropertyFlags properties) {
	VkPhysicalDeviceMemoryProperties memProperties;
	vkGetPhysicalDeviceMemoryProperties(this->physicalDevice, &memProperties);

	// check memory type exists, and has the right properties:
	for (uint32_t i = 0; i < memProperties.memoryTypeCount; ++i) {
		if ((filter & (1 << i)) &&
			(memProperties.memoryTypes[i].propertyFlags & properties) == properties) {
			return i;
		}
	}

	throw std::runtime_error("Could not find a suitable memory type !");
}
// }}}

// Create SEMAPHORES for drawing. It has been 80 pages since 'the last step'. {{{
void BasicApp::createSemaphores() {
	this->imageAvailableSemaphores.resize(MAX_FLIGHT);
	this->renderFinishedSemaphores.resize(MAX_FLIGHT);
	this->flyingFences.resize(MAX_FLIGHT);
	this->imageFences.resize(this->swapchainImageViews.size(), VK_NULL_HANDLE);

	VkSemaphoreCreateInfo sci{};
	sci.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
	sci.flags = 0;
	sci.pNext = nullptr;

	VkFenceCreateInfo fi{};
	fi.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
	fi.flags = VK_FENCE_CREATE_SIGNALED_BIT;

	for (std::size_t i = 0; i < MAX_FLIGHT; ++i) {
		if (vkCreateSemaphore(this->logicalDevice, &sci, nullptr, &this->imageAvailableSemaphores[i]) != VK_SUCCESS) {
			throw std::runtime_error("Cannot create image semaphore");
		}

		if (vkCreateSemaphore(this->logicalDevice, &sci, nullptr, &this->renderFinishedSemaphores[i]) != VK_SUCCESS) {
			throw std::runtime_error("Cannot create render semaphore");
		}

		if (vkCreateFence(this->logicalDevice, &fi, nullptr, &this->flyingFences[i]) != VK_SUCCESS) {
			throw std::runtime_error("Cannot create fence.");
		}
	}
}
// }}}

// Create a shader module {{{
VkShaderModule BasicApp::createShaderModule(const std::vector<char> &bytecode) {
	VkShaderModuleCreateInfo createInfo = {};

	createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	createInfo.codeSize = bytecode.size();
	createInfo.pCode = reinterpret_cast<const uint32_t *>(bytecode.data());

	VkShaderModule shaMod;
	if (vkCreateShaderModule(this->logicalDevice, &createInfo, nullptr, &shaMod) != VK_SUCCESS) {
		throw std::runtime_error("Cannot create shader module");
	}

	return shaMod;
}
// }}}

// Generate mipmaps for an image {{{
void BasicApp::generateMipMaps(VkImage _img, VkFormat _img_format, int32_t tex_width, int32_t tex_height,
							   uint32_t mip_levels) {
	// first of all, check we _can_ generate mipmaps :
	VkFormatProperties _format_properties{};
	vkGetPhysicalDeviceFormatProperties(this->physicalDevice, _img_format, &_format_properties);
	// we create an image with optimal tiling, check VkFormatProperties::optimalTilingFeatures :
	if (!(_format_properties.optimalTilingFeatures & VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_LINEAR_BIT)) {
		throw std::runtime_error("texture image format doesnt support linear blitting !!!");
	}

	VkCommandBuffer _local_buf = this->beginOneTimeCommands();

	VkImageMemoryBarrier _img_barrier{};
	_img_barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
	_img_barrier.image = _img;
	_img_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	_img_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	_img_barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	_img_barrier.subresourceRange.baseArrayLayer = 0;
	_img_barrier.subresourceRange.layerCount = 1;
	_img_barrier.subresourceRange.levelCount = 1;

	int32_t mip_width = tex_width;
	int32_t mip_height = tex_height;

	for (uint32_t i = 1; i < mip_levels; ++i) {
		// Fill struct according to current level : from last image (DST) to new blit (SRC)
		_img_barrier.subresourceRange.baseMipLevel = i - 1;
		_img_barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
		_img_barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
		_img_barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		_img_barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;

		// Setup a barrier for synchronisation across transfers :
		vkCmdPipelineBarrier(_local_buf, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, nullptr,
							 0, nullptr, 1, &_img_barrier);

		// Blit image :
		VkImageBlit _img_blit{};
		_img_blit.srcOffsets[0] = {0, 0, 0};
		_img_blit.srcOffsets[1] = {mip_width, mip_height, 1};
		_img_blit.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		_img_blit.srcSubresource.mipLevel = i - 1;
		_img_blit.srcSubresource.baseArrayLayer = 0;
		_img_blit.srcSubresource.layerCount = 1;
		_img_blit.dstOffsets[0] = {0, 0, 0};
		_img_blit.dstOffsets[1] = {
				mip_width > 1 ? mip_width / 2 : 1,
				mip_height > 1 ? mip_height / 2 : 1,
				1
		};
		_img_blit.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		_img_blit.dstSubresource.mipLevel = i;
		_img_blit.dstSubresource.baseArrayLayer = 0;
		_img_blit.dstSubresource.layerCount = 1;

		// blit the image according to the given specs :
		vkCmdBlitImage(_local_buf, _img, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, _img,
					   VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &_img_blit, VK_FILTER_LINEAR);

		// wait for blit to finish, and transition to shader read optimal :
		_img_barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
		_img_barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		_img_barrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
		_img_barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

		// wait on another barrier :
		vkCmdPipelineBarrier(_local_buf, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0, 0,
							 nullptr, 0, nullptr, 1, &_img_barrier);

		// update mip dimensions for the next level :
		if (mip_width > 1) { mip_width /= 2; }
		if (mip_height > 1) { mip_height /= 2; }
	}

	_img_barrier.subresourceRange.baseMipLevel = mip_levels - 1;
	_img_barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
	_img_barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
	_img_barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
	_img_barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

	// wait for the last blit to completely finish, and re-organize image into a "shader-read-optimal" format :
	vkCmdPipelineBarrier(_local_buf, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0, 0,
						 nullptr, 0, nullptr, 1, &_img_barrier);

	this->endOneTimeCommands(_local_buf);
}
// }}}

// Create a texture buffer {{{
void BasicApp::createTextureImage() {
	//
	// READ IMAGE FROM STB :
	//
	int tex_width, tex_height, tex_channels;
	stbi_uc *pixels = stbi_load(TEXTURE_PATH, &tex_width, &tex_height, &tex_channels, STBI_rgb_alpha);
	if (!pixels) {
		throw std::runtime_error("Cannot open texture file !");
	}
	this->mipLevels = static_cast<uint32_t>(std::floor(std::log2(std::max(tex_width, tex_height))));

	// number of pixels (and of bytes, since encoded in uchars :
	VkDeviceSize img_size = tex_width * tex_height * 4;

	//
	// UPLOAD TO STAGING BUFFER :
	//
	VkBuffer staging_buffer;
	VkDeviceMemory staging_buffer_memory;
	this->createBuffer(img_size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
					   VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
					   staging_buffer, staging_buffer_memory);

	void *data;
	vkMapMemory(this->logicalDevice, staging_buffer_memory, 0, img_size, 0, &data);
	memcpy(data, pixels, static_cast<uint32_t>(img_size));
	vkUnmapMemory(this->logicalDevice, staging_buffer_memory);
	// free image host-side, don't need it anymore :
	stbi_image_free(pixels);

	// create the image object :
	this->createImage(tex_width, tex_height, this->mipLevels, VK_SAMPLE_COUNT_1_BIT, VK_FORMAT_R8G8B8A8_SRGB,
					  VK_IMAGE_TILING_OPTIMAL,
					  VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT,
					  VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
					  this->textureImage, this->textureImageMemory);
	// NOTE : generating mipmaps requires the image to have both a TRANSFER_SRC, and a TRANSFER_DST flag for its usage.

	// transition to suitable format, and upload the image data from the staging buffer to the image object :
	this->transitionImageLayout(this->textureImage, VK_FORMAT_R8G8B8A8_SRGB, VK_IMAGE_LAYOUT_UNDEFINED,
								VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, this->mipLevels);
	this->copyBufferToImage(staging_buffer, this->textureImage, static_cast<uint32_t>(tex_width),
							static_cast<uint32_t>(tex_height));
	// at the end, transition to a format suitable for read-only shader operations :
	// this->transitionImageLayout(this->textureImage, VK_FORMAT_R8G8B8A8_SRGB, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, this->mipLevels);
	// right now, generate mipmaps and wait for them to finish :
	this->generateMipMaps(this->textureImage, VK_FORMAT_R8G8B8A8_SRGB, tex_width, tex_height, this->mipLevels);

	vkDestroyBuffer(this->logicalDevice, staging_buffer, nullptr);
	vkFreeMemory(this->logicalDevice, staging_buffer_memory, nullptr);
}
// }}}

// Create a texture's view {{{
void BasicApp::createTextureView() {
	this->textureImageView = this->createImageView(this->textureImage, VK_FORMAT_R8G8B8A8_SRGB,
												   VK_IMAGE_ASPECT_COLOR_BIT, this->mipLevels);
}
// }}}

// Create a texture sampler {{{
void BasicApp::createTextureSampler() {
	VkSamplerCreateInfo _sampler_info{};
	_sampler_info.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
	_sampler_info.magFilter = VK_FILTER_LINEAR;
	_sampler_info.minFilter = VK_FILTER_LINEAR;
	_sampler_info.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	_sampler_info.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	_sampler_info.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	_sampler_info.anisotropyEnable = VK_TRUE;
	_sampler_info.maxAnisotropy = 16;
	_sampler_info.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
	_sampler_info.unnormalizedCoordinates = VK_FALSE; // can be true to get coordinates in [0, width][0, height] !!!
	_sampler_info.compareEnable = VK_FALSE;            // Those two params are used in
	_sampler_info.compareOp = VK_COMPARE_OP_ALWAYS;    // conditional filtering, seen later
	_sampler_info.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
	_sampler_info.mipLodBias = 0.0f;
	_sampler_info.minLod = 0.0f;
	_sampler_info.maxLod = static_cast<float>(this->mipLevels);

	if (vkCreateSampler(this->logicalDevice, &_sampler_info, nullptr, &this->textureSampler) != VK_SUCCESS) {
		throw std::runtime_error("Could not create texture sampler !");
	}
}
// }}}

// Create the depth buffer's resources {{{
void BasicApp::createDepthBuffer() {
	VkFormat depth_format = this->findDepthFormat();

	this->createImage(this->swapchainExtent.width, this->swapchainExtent.height, 1, this->msaaSamples, depth_format,
					  VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
					  VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, this->depthImage, this->depthImageMemory);

	this->depthImageView = this->createImageView(this->depthImage, depth_format, VK_IMAGE_ASPECT_DEPTH_BIT, 1);

	// Optional, but nice addition :
	// Explicitly waits for the transform of the depth/stencil buffer from undefined contents to a state where the depth
	// read/writes are optimal (defined by the driver)
	// this->transitionImageLayout(this->depthImage, depth_format, VkImageLayout::VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL);
	// WARNING : Since called before framebuffers and command pools were allocated, segfault the program.
}
// }}}

// Creates the render target used for MSAA {{{
void BasicApp::createMSAARenderTarget() {
	VkFormat _msaa_color_format = this->swapchainImageFormat;

	this->createImage(this->swapchainExtent.width, this->swapchainExtent.height, 1, this->msaaSamples,
					  _msaa_color_format,
					  VK_IMAGE_TILING_OPTIMAL,
					  VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT | VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
					  VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, this->colorImage, this->colorImageMemory);
	this->colorImageView = this->createImageView(this->colorImage, _msaa_color_format, VK_IMAGE_ASPECT_COLOR_BIT, 1);
}
// }}}

// Find a suitable format, which has a given set of features {{{
VkFormat BasicApp::findSupportedFormat(const std::vector<VkFormat> &candidates, VkImageTiling tiling,
									   VkFormatFeatureFlags features) {
	for (VkFormat format: candidates) {
		VkFormatProperties format_props;
		vkGetPhysicalDeviceFormatProperties(this->physicalDevice, format, &format_props);

		if (tiling == VK_IMAGE_TILING_LINEAR && ((format_props.linearTilingFeatures & features) == features)) {
			return format;
		} else if (tiling == VK_IMAGE_TILING_OPTIMAL && ((format_props.optimalTilingFeatures & features) == features)) {
			return format;
		}
	}
	// none found, bad time to crash :(
	throw std::runtime_error("No suitable format found.");
}
// }}}

// Find a suitable depth format {{{
VkFormat BasicApp::findDepthFormat() {
	return this->findSupportedFormat({VK_FORMAT_D32_SFLOAT, VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT},
									 VK_IMAGE_TILING_OPTIMAL, VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT);
}
// }}}

// [Generic] Create an image {{{
void BasicApp::createImage(uint32_t width, uint32_t height, uint32_t _mip_levels, VkSampleCountFlagBits num_samples,
						   VkFormat _img_format, VkImageTiling _img_tiling,
						   VkImageUsageFlags _img_usage, VkMemoryPropertyFlags _img_properties, VkImage &_img,
						   VkDeviceMemory &_img_memory) {
	//
	// CREATE IMAGE OBJECT IN VULKAN :
	//
	VkImageCreateInfo _img_create_info{};
	_img_create_info.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
	_img_create_info.imageType = VK_IMAGE_TYPE_2D;
	_img_create_info.extent.width = width;
	_img_create_info.extent.height = height;
	_img_create_info.extent.depth = 1;
	_img_create_info.mipLevels = _mip_levels;
	_img_create_info.arrayLayers = 1;
	_img_create_info.format = _img_format;
	_img_create_info.tiling = _img_tiling;
	_img_create_info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	_img_create_info.usage = _img_usage; // this is the destination
	// of the buffer data, and we want to sample the image later
	_img_create_info.samples = num_samples;
	_img_create_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	// create image object :
	if (vkCreateImage(this->logicalDevice, &_img_create_info, nullptr, &_img) != VK_SUCCESS) {
		throw std::runtime_error("Cannot create image !!!");
	}

	//
	// ALLOCATE MEMORY FOR THE PIXELS :
	//
	VkMemoryRequirements _mem_requirements{};
	vkGetImageMemoryRequirements(this->logicalDevice, _img, &_mem_requirements);
	VkMemoryAllocateInfo _alloc_info{};
	_alloc_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	_alloc_info.allocationSize = _mem_requirements.size;
	_alloc_info.memoryTypeIndex = this->findMemoryType(_mem_requirements.memoryTypeBits, _img_properties);
	if (vkAllocateMemory(this->logicalDevice, &_alloc_info, nullptr, &_img_memory) != VK_SUCCESS) {
		throw std::runtime_error("Cannot allocate image memory !!!");
	}
	// bind memory region and image object :
	vkBindImageMemory(this->logicalDevice, _img, _img_memory, 0);
}
// }}}

// [Generic] Create an image view {{{
VkImageView
BasicApp::createImageView(VkImage _img, VkFormat _img_format, VkImageAspectFlags _img_aspect, uint32_t mip_levels) {
	VkImageViewCreateInfo _img_create_info{};
	_img_create_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	_img_create_info.image = _img;
	_img_create_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
	_img_create_info.format = _img_format;
	_img_create_info.subresourceRange.aspectMask = _img_aspect;
	_img_create_info.subresourceRange.baseMipLevel = 0;
	_img_create_info.subresourceRange.levelCount = mip_levels;
	_img_create_info.subresourceRange.baseArrayLayer = 0;
	_img_create_info.subresourceRange.layerCount = 1;

	VkImageView _img_view;
	if (vkCreateImageView(this->logicalDevice, &_img_create_info, nullptr, &_img_view) != VK_SUCCESS) {
		throw std::runtime_error("Could not create the image view [generic] !!!");
	}

	return _img_view;
}
// }}}

// Begin a one-time command buffer {{{
VkCommandBuffer BasicApp::beginOneTimeCommands() {
	// allocate the buffer :
	VkCommandBuffer _cmd_buf;
	VkCommandBufferAllocateInfo _alloc_info{};
	_alloc_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	_alloc_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	_alloc_info.commandPool = this->commandPool;
	_alloc_info.commandBufferCount = 1;
	vkAllocateCommandBuffers(this->logicalDevice, &_alloc_info, &_cmd_buf);

	// Begin recording the buffer :
	VkCommandBufferBeginInfo _cmd_begin{};
	_cmd_begin.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	_cmd_begin.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;    //only used once, and forgotten afterwards
	vkBeginCommandBuffer(_cmd_buf, &_cmd_begin);

	// return the buffer :
	return _cmd_buf;
}
// }}}

// End a one-time command buffer {{{
void BasicApp::endOneTimeCommands(VkCommandBuffer _buffer) {
	vkEndCommandBuffer(_buffer);

	VkSubmitInfo _si{};
	_si.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	_si.commandBufferCount = 1;
	_si.pCommandBuffers = &_buffer;

	vkQueueSubmit(this->graphicsQueue, 1, &_si, VK_NULL_HANDLE);
	// note : if we do multiple transfers at a time, specify a fence in the queue submit and wait on
	// that instead of the queue being idle. might give rise to better optimisation by the driver.

	// We want our buffer transfer to be synchronous with the
	// function call, thus we wait for the queue to be idle :
	vkQueueWaitIdle(this->graphicsQueue);
	vkFreeCommandBuffers(this->logicalDevice, this->commandPool, 1, &_buffer);
}
// }}}

// Handle image layouts transitions {{{
void BasicApp::transitionImageLayout(VkImage _img, VkFormat _img_format, VkImageLayout _old_layout,
									 VkImageLayout _new_layout, uint32_t mip_levels) {
	VkCommandBuffer _local_buf = this->beginOneTimeCommands();

	VkImageMemoryBarrier _img_barrier{};
	_img_barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
	_img_barrier.oldLayout = _old_layout;
	_img_barrier.newLayout = _new_layout;
	_img_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	_img_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	// specifies the image affected, and which parts of it more specifically :
	_img_barrier.image = _img;

	if (_new_layout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL) {
		_img_barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
		if (this->has_stencil_component(_img_format)) {
			_img_barrier.subresourceRange.aspectMask |= VK_IMAGE_ASPECT_STENCIL_BIT;
		}
	} else {
		_img_barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	}

	_img_barrier.subresourceRange.baseMipLevel = 0;
	_img_barrier.subresourceRange.levelCount = mip_levels;
	_img_barrier.subresourceRange.baseArrayLayer = 0;
	_img_barrier.subresourceRange.layerCount = 1;

	// Set the access and destination access masks based on the old and new layouts :
	VkPipelineStageFlags _src_stage, _dst_stage;
	if (_old_layout == VK_IMAGE_LAYOUT_UNDEFINED && _new_layout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
		_img_barrier.srcAccessMask = 0;
		_img_barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		_src_stage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
		_dst_stage = VK_PIPELINE_STAGE_TRANSFER_BIT;
	} else if (_old_layout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL &&
			   _new_layout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
		_img_barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		_img_barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
		_src_stage = VK_PIPELINE_STAGE_TRANSFER_BIT;
		_dst_stage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
	} else if (_old_layout == VK_IMAGE_LAYOUT_UNDEFINED &&
			   _new_layout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL) {
		_img_barrier.srcAccessMask = 0;
		_img_barrier.dstAccessMask =
				VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
		_src_stage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
		_dst_stage = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
	} else {
		throw std::invalid_argument("unsupported layout transition !!!");
	}

	// enable the barrier :
	vkCmdPipelineBarrier(_local_buf,
						 _src_stage,
						 _dst_stage,
						 0,
						 0, nullptr,
						 0, nullptr,
						 1, &_img_barrier);

	this->endOneTimeCommands(_local_buf);
}
// }}}

// Copies buffer data into an image {{{
void BasicApp::copyBufferToImage(VkBuffer _src_buf, VkImage _dst_img, uint32_t width, uint32_t height) {
	VkCommandBuffer _local_buf = this->beginOneTimeCommands();

	// Copy the buffer data, as specified by this struct :
	VkBufferImageCopy _copy_region{};
	// Buffer-side offsets and data :
	_copy_region.bufferOffset = 0;
	_copy_region.bufferRowLength = 0; // 0 means the pixels are tightly packed in a row
	_copy_region.bufferImageHeight = 0; // idem above, but for image rows themselves
	// Image resource information :
	_copy_region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	_copy_region.imageSubresource.mipLevel = 0;
	_copy_region.imageSubresource.baseArrayLayer = 0;
	_copy_region.imageSubresource.layerCount = 1;
	// Physical pixel regions :
	_copy_region.imageOffset = {0, 0, 0};
	_copy_region.imageExtent = {width, height, 1};

	// Launch the copy operation :
	vkCmdCopyBufferToImage(_local_buf, _src_buf, _dst_img, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &_copy_region);

	this->endOneTimeCommands(_local_buf);
}
// }}}

// Utilities for the swapchain creation {{{
VkSurfaceFormatKHR BasicApp::chooseSwapchainSupport(const Vulkan::DeviceFilter::PhyDev &dev) {
	for (const auto &sf: dev.details.formats) {
		if (sf.format == VK_FORMAT_R8G8B8A8_SRGB && sf.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
			std::cerr << "Found a right swapchain format! R8G8B8 and nonlinear colorspace." << '\n';
			return sf;
		}
	}

	return dev.details.formats[0];
}

VkPresentModeKHR BasicApp::chooseSwapchainPresentMode(const Vulkan::DeviceFilter::PhyDev &dev) {
	for (const auto &pm: dev.details.presentMode) {
		if (pm == VK_PRESENT_MODE_MAILBOX_KHR) {
			return pm;
		}
	}
	return VK_PRESENT_MODE_FIFO_KHR;
}

VkExtent2D BasicApp::chooseSwapchainExtent(const Vulkan::DeviceFilter::PhyDev &dev) {
	if (dev.details.capabilities.currentExtent.width != UINT32_MAX) {
		return dev.details.capabilities.currentExtent;
	} else {
		VkExtent2D e = {
				static_cast<uint32_t>(this->width),
				static_cast<uint32_t>(this->height)
		};

		return e;
	}
}
// }}}

// Create VkInstance {{{
void BasicApp::createVulkanInstance() {
	// App creation information :
	// Fill in the app's info {{{
	VkApplicationInfo appInfo{
		.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
		.pNext = nullptr,
		.pApplicationName = "Basic App : Hello, world.",
		.applicationVersion = VK_MAKE_VERSION(0, 0, 1),
		.pEngineName = "No Engine",
		.engineVersion = VK_MAKE_VERSION(0, 0, 1),
		.apiVersion = VK_API_VERSION_1_1,
	};
	// }}}

	std::vector<const char *> enabledExtensions = {};

	// Get the extensions required by GLFW {{{
	const uint32_t glfwExtensionCount = 3;
	const char *glfwExtensions[glfwExtensionCount] = {
		this->enabledDebug ? VK_EXT_DEBUG_UTILS_EXTENSION_NAME : "", /* it's actually a device extension! */
		"VK_KHR_surface",
		// "VK_KHR_xlib_surface",
		// "VK_KHR_xcb_surface",
		"VK_KHR_wayland_surface",
	};
	// glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);
	// Print the extensions :
	std::cerr << "Qt required " << glfwExtensionCount << " extensions" << '\n';
	for (uint32_t i = 0; i < glfwExtensionCount; ++i) {
		std::cerr << "Qt extension required : " << glfwExtensions[i] << '\n';
	}
	// }}}

	// Get all required extensions (user & GLFW) {{{
	// Get all available Vk extensions :
	std::vector<VkExtensionProperties> extProps = Vulkan::Enumerators::instanceExtensionProperties(nullptr);
	std::vector<bool> printed_rejections(extProps.size(), false);
	// Add required GLFW extensions :
	for (uint32_t i = 0; i < glfwExtensionCount; ++i) {
		int j = 0;
		for (VkExtensionProperties vkep: extProps) {
			if (strcmp(vkep.extensionName, glfwExtensions[i]) == 0) {
				enabledExtensions.push_back(glfwExtensions[i]);
				std::cerr << "[ENABLED] Available instance extension: \"" << vkep.extensionName << "\"\n";
				break;
			}
			if (printed_rejections[j] == false) {
				std::cerr << "Available instance extension: \"" << vkep.extensionName << "\"\n";
				printed_rejections[j] = true;
			}
			j++;
		}
	}

	// Get all available Vk layers :
	std::vector<VkLayerProperties> layerProps = Vulkan::Enumerators::instanceLayerProperties();
	std::vector<const char *> enabledLayers = {};
	// Add user-required layers :
	for (const char *pLayerName: this->requiredLayers) {
		bool found = false;
		for (VkLayerProperties vklp: layerProps) {
			if (strcmp(vklp.layerName, pLayerName) == 0) {
				enabledLayers.push_back(pLayerName);
				found = true;
				break;
			}
		}
		if (not found) {
			throw std::runtime_error("Could not initialize the app with all required layers\n");
		}
	}

	std::cerr << "===========\n";
	std::cerr << "Enabled extensions in this VkInstance : \n";
	for (const char *pExtName: enabledExtensions) {
		std::cerr << "\t- " << pExtName << '\n';
	}
	std::cerr << "Enabled layers in this VkInstance : \n";
	for (const char *pLayerName: enabledLayers) {
		std::cerr << "\t- " << pLayerName << '\n';
	}
	std::cerr << "===========\n";
	// }}}

	// Instance creation information :
	VkInstanceCreateInfo createInfo{};
	// Fill in the vk instance info {{{
	createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	createInfo.pNext = nullptr;
	createInfo.flags = 0;
	// App info :
	createInfo.pApplicationInfo = &appInfo;
	// Layers :
	createInfo.enabledLayerCount = static_cast<uint32_t>(enabledLayers.size());
	createInfo.ppEnabledLayerNames = enabledLayers.data();
	// Extensions :
	createInfo.enabledExtensionCount = static_cast<uint32_t>(enabledExtensions.size());
	createInfo.ppEnabledExtensionNames = enabledExtensions.data();
	// }}}

	VkDebugUtilsMessengerCreateInfoEXT debugCreateInfo{};
	if (this->enabledDebug) {
		this->createDebugInfoStruct(debugCreateInfo);
		createInfo.pNext = (VkDebugUtilsMessengerCreateInfoEXT *) &debugCreateInfo;
	}

	VkResult instanceCreationResult = vkCreateInstance(&createInfo, nullptr, &this->instance);

	if (instanceCreationResult != VK_SUCCESS) {
		std::cerr << "Something happened while creating the instance." << '\n';
		Vulkan::Strings::printResultType(instanceCreationResult);
		throw std::runtime_error("BasicApp::createVulkanInstance()\nCould not create a VkInstance object.");
	}
}
// }}}

// Fill in the debug messenger create info struct {{{
void BasicApp::createDebugInfoStruct(VkDebugUtilsMessengerCreateInfoEXT &createInfo) {
	createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
	createInfo.pNext = nullptr;
	createInfo.flags = 0;
	createInfo.messageSeverity =
			// VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT |
			// VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT |
			VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
			VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
	createInfo.messageType =
			VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
			VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
			VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
	createInfo.pfnUserCallback = debugCallback;
	createInfo.pUserData = nullptr;
}
// }}}

// Setup the debug messenger {{{
void BasicApp::setupDebugMessenger() {
	VkDebugUtilsMessengerCreateInfoEXT createInfo{};
	this->createDebugInfoStruct(createInfo);

	VkResult debugMessengerCreationResult =
			CreateDebugUtilsMessengerEXT(this->instance, &createInfo, nullptr, &this->debugMessenger);

	if (debugMessengerCreationResult != VK_SUCCESS) {
		std::cerr << "Could not create a debug messenger." << '\n';
		Vulkan::Strings::printResultType(debugMessengerCreationResult);
		throw std::runtime_error("Could not create a debug messenger.\n");
	}
}
// }}}

// Debug callback for the VkInstance {{{
VKAPI_ATTR VkBool32 VKAPI_CALL BasicApp::debugCallback(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
													   VkDebugUtilsMessageTypeFlagsEXT messageType,
													   const VkDebugUtilsMessengerCallbackDataEXT *pCallbackData,
													   void *pUserData) {
	PRINT_FN;
	std::cerr << "[" << Vulkan::Strings::getMessageSeverity(messageSeverity) << "]" <<
			  "[" << Vulkan::Strings::getMessageType(messageType) << "]";

	std::cerr << "[ID:" << pCallbackData->messageIdNumber << "]" <<
			  "[" << pCallbackData->pMessageIdName << "]" <<
			  " \n" << pCallbackData->pMessage << '\n';
	if (pCallbackData->objectCount > 0) {
		std::cerr << pCallbackData->objectCount << " objects :\n";
		for (uint32_t i = 0; i < pCallbackData->objectCount; ++i) {
			std::string objName = (pCallbackData->pObjects[i].pObjectName != nullptr) ?
								  pCallbackData->pObjects[i].pObjectName : "<no name>";
			std::cerr << "\t[" << pCallbackData->pObjects[i].objectHandle << "] " <<
					  Vulkan::Strings::getObjectType(pCallbackData->pObjects[i].objectType) <<
					  " " << objName << '\n';
		}
	}
	PRINT_FN;

	return VK_FALSE;
}
// }}}

