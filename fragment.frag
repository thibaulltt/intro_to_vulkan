#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location=0) in  vec3 vertColor;
layout(location=1) in  vec2 fragTexCoord;
layout(location=0) out vec4 outColor;

layout(binding=1) uniform sampler2D texSampler;

vec3 apply_gamma_correction(in vec3 original_color, in float gamma_exponent);

void main() {
	vec4 vertexColor = vec4(fragTexCoord, 0., 1.);
	vec3 raw_textureColor = texture(texSampler, fragTexCoord).rgb;
	vec4 textureColor = vec4(apply_gamma_correction(raw_textureColor, 2.f), 1.f);
	/*
	const float mix_ratio = 0.5f;
	outColor = vec4(((1.f - mix_ratio)*vertexColor.rgb + mix_ratio*textureColor.rgb), 1.f);
	*/
	outColor = textureColor;
}

vec3 apply_gamma_correction(in vec3 original_color, in float gamma_exponent) {
	float gamma_ratio = 1.f / gamma_exponent;
	original_color.r = pow(original_color.r, gamma_ratio);
	original_color.g = pow(original_color.g, gamma_ratio);
	original_color.b = pow(original_color.b, gamma_ratio);
	return original_color;
}
