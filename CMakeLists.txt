CMAKE_MINIMUM_REQUIRED (VERSION 3.0)
PROJECT (vulkan_testing)

# CMake Setup (compile_commands, debug mode check, and build directory check) {{{
IF(CMAKE_BINARY_DIR STREQUAL CMAKE_SOURCE_DIR)
	MESSAGE(FATAL_ERROR "Please select another Build Directory !\nWe recommend doing an out-of-source build. Create a build folder\nhere, or in the parent root, but don't build the project here !")
ENDIF()

IF(WIN32 OR MINGW OR MSVC)
        MESSAGE(FATAL_ERROR "This project cannot be compiled on a Windows platform.")
ENDIF()

SET(CMAKE_EXPORT_COMPILE_COMMANDS TRUE)
SET(CMAKE_CXX_STANDARD_REQUIRED TRUE)
SET(CMAKE_USE_RELATIVE_PATHS TRUE)

SET(DEFAULT_BUILD_TYPE "Release")
IF(NOT CMAKE_BUILD_TYPE AND NOT CMAKE_CONFIGURATION_TYPES)
	MESSAGE(STATUS "Setting build type to '${DEFAULT_BUILD_TYPE}' as none was specified.")
	SET(CMAKE_BUILD_TYPE "${DEFAULT_BUILD_TYPE}" CACHE
		STRING "Choose the type of build." FORCE)
	# Set the possible values of build type for cmake-gui
	SET_PROPERTY(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS "Debug" "Release" "MinSizeRel" "RelWithDebInfo")
ENDIF()

IF(CMAKE_BUILD_TYPE STREQUAL "Debug")
	SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O0 -ggdb")
ENDIF()

MESSAGE(STATUS "Currently compiling in ${CMAKE_BUILD_TYPE} mode.")

FIND_PACKAGE(glfw3 REQUIRED)
FIND_PACKAGE(glm REQUIRED)
FIND_PACKAGE(Vulkan REQUIRED)

FIND_PACKAGE(Qt6 REQUIRED COMPONENTS Core Gui Widgets WaylandClient)
# }}}

# Find STB in the system headers {{{
SET(STB_SEARCH_PATHS $ENV{PATH} /usr/include /usr/local/include)
FIND_PATH(STB_INCLUDE_DIRECTORY
		NAMES stb_image.h
		PATHS ${STB_SEARCH_PATHS}
		PATH_SUFFIXES stb
		DOC "The STB main file"
)
MESSAGE(STATUS "Using STB header directory as ${STB_INCLUDE_DIRECTORY}")
IF(NOT EXISTS ${STB_INCLUDE_DIRECTORY})
	MESSAGE(FATAL_ERROR "Cannot find STB !")
ENDIF()

# }}}

ADD_LIBRARY(vk_strings
    vk_strings.cpp
    vk_strings.hpp
)
TARGET_LINK_LIBRARIES(vk_strings
    PUBLIC Vulkan::Vulkan
)

ADD_LIBRARY(vk_basic_app
	basic_app.cpp
	basic_app.hpp
	enumerators.cpp
	enumerators.hpp
	device_filter.cpp
	device_filter.hpp
	vertex_specification.hpp
)
TARGET_INCLUDE_DIRECTORIES(vk_basic_app
	PRIVATE ${GLM_INCLUDE_DIRS}
	PRIVATE ${GLFW_INCLUDE_DIRS}
	PRIVATE ${STB_INCLUDE_DIRECTORY}
)
TARGET_LINK_LIBRARIES(vk_basic_app
	PUBLIC glfw
	PUBLIC glm
	PUBLIC Vulkan::Vulkan
	PUBLIC vk_strings
)
SET_TARGET_PROPERTIES(vk_basic_app PROPERTIES
	CXX_STANDARD 17
	AUTOMOC TRUE
)

add_executable(vkwindow
	main.cpp
	vkwindow.hpp
	vkwindow.cpp
)
target_link_libraries(vkwindow
	PUBLIC vk_basic_app
	PUBLIC Qt6::Core
	PUBLIC Qt6::Gui
	PUBLIC Qt6::Widgets
	PUBLIC Qt6::WaylandClient
)
set_target_properties(vkwindow PROPERTIES
	CXX_STANDARD 17
	AUTOMOC TRUE
)

# vim: foldmethod=marker : foldmarker={{{,}}} : tabstop=8
