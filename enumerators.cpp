#include "./enumerators.hpp"

#include <cstring>
#include <iomanip>
#include <cmath>
#include <vulkan/vulkan_core.h>

namespace Vulkan {
namespace Enumerators {

	// Extension properties enumerator (can be filtered by layer name) {{{
	std::vector<VkExtensionProperties> instanceExtensionProperties(const char* pLayerName) {
		// get prop count :
		uint32_t count = 0;
		std::vector<VkExtensionProperties> extensionProps;
		vkEnumerateInstanceExtensionProperties(pLayerName, &count, nullptr);

		if (count != 0) {
			// get all extension props :
			extensionProps.resize(count);
			vkEnumerateInstanceExtensionProperties(pLayerName, &count, extensionProps.data());
		}

		return extensionProps;
	}

	std::vector<VkExtensionProperties> deviceExtensionProperties(const VkPhysicalDevice& physicalDevice, const char* pLayerName) {
		// get prop count :
		uint32_t count = 0;
		std::vector<VkExtensionProperties> extensionProps;
		vkEnumerateDeviceExtensionProperties(physicalDevice, pLayerName, &count, nullptr);

		if (count != 0) {
			// get all extension props :
			extensionProps.resize(count);
			vkEnumerateDeviceExtensionProperties(physicalDevice, pLayerName, &count, extensionProps.data());
		}

		return extensionProps;
	}
	// }}}

	// Layers available on the system for a new instance {{{
	std::vector<VkLayerProperties> instanceLayerProperties() {
		uint32_t count = 0;
		std::vector<VkLayerProperties> layerProperties;
		vkEnumerateInstanceLayerProperties(&count, nullptr);

		if (count != 0) {
			layerProperties.resize(count);
			vkEnumerateInstanceLayerProperties(&count, layerProperties.data());
		}

		return layerProperties;
	}
	// }}}

	// Physical devices available on the system {{{
	std::vector<VkPhysicalDevice> physicalDevices(VkInstance instance) {
		std::vector<VkPhysicalDevice> phyDevs = {};
		uint32_t count = 0;
		vkEnumeratePhysicalDevices(instance, &count, nullptr);
		if (count == 0) {
			throw std::runtime_error("There are no physical devices available on the system !\n");
		}
		phyDevs.resize(count);
		vkEnumeratePhysicalDevices(instance, &count, phyDevs.data());
		return phyDevs;
	}
	// }}}

// Available queue families for a physical device {{{
std::vector<VkQueueFamilyProperties> queueProperties(const VkPhysicalDevice& dev) {
	uint32_t queueCount = 0;
	vkGetPhysicalDeviceQueueFamilyProperties(dev, &queueCount, nullptr);

	std::vector<VkQueueFamilyProperties> fam;
	fam.resize(queueCount);
	if (queueCount) { vkGetPhysicalDeviceQueueFamilyProperties(dev, &queueCount, fam.data()); }

	return fam;
}
// }}}

// Extensions for a given device {{{
std::vector<VkExtensionProperties> extensionProperties(const VkPhysicalDevice& dev) {
	uint32_t count = 0;
	vkEnumerateDeviceExtensionProperties(dev, nullptr, &count, nullptr);

	std::vector<VkExtensionProperties> ext;
	if (count != 0) {
		ext.resize(count);
		vkEnumerateDeviceExtensionProperties(dev, nullptr, &count, ext.data());
	}

	return ext;
}
// }}}

std::vector<VkSurfaceFormatKHR> surfaceFormats(const VkPhysicalDevice& dev, const VkSurfaceKHR& surf) {
	uint32_t count = 0;
	vkGetPhysicalDeviceSurfaceFormatsKHR(dev, surf, &count, nullptr);

	std::vector<VkSurfaceFormatKHR> formats;
	if (count != 0) {
		formats.resize(count);
		vkGetPhysicalDeviceSurfaceFormatsKHR(dev, surf, &count, formats.data());
	}

	return formats;
}

std::vector<VkPresentModeKHR> presentModes(const VkPhysicalDevice& dev, const VkSurfaceKHR& surf) {
	uint32_t count = 0;
	vkGetPhysicalDeviceSurfacePresentModesKHR(dev, surf, &count, nullptr);

	std::vector<VkPresentModeKHR> presentModes;
	if (count != 0) {
		presentModes.resize(count);
		vkGetPhysicalDeviceSurfacePresentModesKHR(dev, surf, &count, presentModes.data());
	}

	return presentModes;
}

} // namespace Enumerators
} // namespace Vulkan
