#ifndef VULKAN_TESTING_VERTEX_SPECIFICATION_HPP
#define VULKAN_TESTING_VERTEX_SPECIFICATION_HPP

#include "./vk.hpp"

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/hash.hpp>
#include <glm/glm.hpp>
#include <vector>
#include <array>

struct Vertex {
    glm::vec3 pos;
    glm::vec3 color;
    glm::vec2 texCoord;

    static VkVertexInputBindingDescription getBindingDescription() {
        VkVertexInputBindingDescription bindingDescription{};

        bindingDescription.binding = 0;                             // binding point 0
        bindingDescription.stride = sizeof(Vertex);                 // size bytes from one element to the next
        bindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX; // specifies to move to the next entry in the list
        // after each vertex. Can also have the value VK_VERTEX_INPUT_RATE_INSTANCE, in which case it will move onto the
        // next value at each instance (akin to calling `glVertexAttribDivisor()` on a VBO).

        return bindingDescription;
    }

    static std::array<VkVertexInputAttributeDescription , 3> getAttributeDescription() {
        std::array<VkVertexInputAttributeDescription, 3> attributeDescriptions{};

        attributeDescriptions[0].binding = 0;   /// source binding of the binding description
        attributeDescriptions[0].location = 0;  /// destination location() in the vertex shader
        attributeDescriptions[0].format = VK_FORMAT_R32G32B32_SFLOAT;  // data type, generic RGB 32bit fp (non-normalized)
        attributeDescriptions[0].offset = offsetof(Vertex, pos); // position of the attribute described here within the struct

        attributeDescriptions[1].binding = 0;
        attributeDescriptions[1].location = 1;
        attributeDescriptions[1].format = VK_FORMAT_R32G32B32_SFLOAT;
        attributeDescriptions[1].offset = offsetof(Vertex, color);

        attributeDescriptions[2].binding = 0;
		attributeDescriptions[2].location = 2;
		attributeDescriptions[2].format = VK_FORMAT_R32G32_SFLOAT;
		attributeDescriptions[2].offset = offsetof(Vertex, texCoord);

        return attributeDescriptions;
    }

    bool operator==(const Vertex& other) const {
    	return pos == other.pos && color == other.color && texCoord == other.texCoord;
    }
};

namespace std{
	template<> struct hash<Vertex> {
		size_t operator()(Vertex const& vertex) const {
			return ((hash<glm::vec3>()(vertex.pos) ^ (hash<glm::vec3>()(vertex.color) << 1)) >> 1) ^
					(hash<glm::vec2>()(vertex.texCoord) << 1);
		}
	};
}

const std::vector<Vertex> vertices_ = {
	// top square :
	{{-.5f, -.5f,  0.f}, {1.f, 0.f, 0.f}, {0.0f, 0.0f}},
	{{0.5f, -.5f,  0.f}, {0.f, 1.f, 0.f}, {1.0f, 0.0f}},
	{{0.5f, 0.5f,  0.f}, {0.f, 0.f, 1.f}, {1.0f, 1.0f}},
	{{-.5f, 0.5f,  0.f}, {1.f, 1.f, 1.f}, {0.0f, 1.0f}},
	// bottom square :
	{{-.5f, -.5f, -.5f}, {1.f, 0.f, 0.f}, {0.0f, 0.0f}},
	{{0.5f, -.5f, -.5f}, {0.f, 1.f, 0.f}, {1.0f, 0.0f}},
	{{0.5f, 0.5f, -.5f}, {0.f, 0.f, 1.f}, {1.0f, 1.0f}},
	{{-.5f, 0.5f, -.5f}, {1.f, 1.f, 1.f}, {0.0f, 1.0f}},
};

/// @b Indices of the vertices to draw.
const std::vector<uint16_t> vert_indices_ = {
	0, 1, 2, 2, 3, 0,
	4, 5, 6, 6, 7, 4,
};

struct UniformBufferObject {
	alignas (16) glm::mat4 model;	///< The model matrix
	alignas (16) glm::mat4 view;		///< The view matrix
	alignas (16) glm::mat4 proj;		///< The projection matrix
};

typedef UniformBufferObject UBO; ///< Shortcut to UniformBufferObject

#endif //VULKAN_TESTING_VERTEX_SPECIFICATION_HPP
