#ifndef VKINTRO_BASIC_APP_HPP_
#define VKINTRO_BASIC_APP_HPP_

#include "./vk.hpp"
#include "./enumerators.hpp"
#include "./device_filter.hpp"
#include "vertex_specification.hpp"
#include <vulkan/vk_platform.h>
#include <vulkan/vulkan_core.h>
#include <memory>
#include <thread>

#define WIN_WIDTH 1366
#define WIN_HEIGHT 768
#define MAX_FLIGHT 2
#define MODEL_PATH "../../models/viking_room.obj"
#define TEXTURE_PATH "../../models/viking_room.png"

/// @brief The main function of the `BasicApp::optional_modifying_thread` member variable.
/// @param instance: The instance to access the buffers to modify
/// @param logical_device: The device to fetch memory from
/// @param physical_device: The physical device where memory is located
/// @param _buffer_to_modify: The buffer to read and modify
/// @param _buffer_memory_to_modify: The buffer memory pool where the data is stored
/// @param transformer_function: The function applied to all elements of the buffer.
/// @tparam transformer_return_type: The return type of the transformer function.
/// @tparam transformer_parameter_type: The singular parameter type of the transformer function.
template <typename transformer_return_type, typename transformer_parameter_type>
void optional_modifying_thread_main(
	VkInstance instance,
	VkDevice logical_device,
	VkPhysicalDevice physical_device,
	VkBuffer _buffer_to_modify,
	VkDeviceMemory _buffer_memory_to_modify,
	std::function<transformer_return_type(transformer_parameter_type)> transformer_function
);

class BasicApp {
	public:
		/// @brief Create an app, with no default parameters.
		BasicApp();
		/// @brief Destroy the application's instance, and free up its resources.
		~BasicApp();

		/// @brief Require layers *before* initializing the app.
		BasicApp& requireLayers(std::initializer_list<const char *> layers);
		/// @brief Enables or disables the debug extension for Vulkan.
		BasicApp& enableDebugExt(bool enabled = true);
		/// @brief Initialize the application.
		void init();
		/// @brief Initialize the Vulkan API, for the application.
		void initVulkan();
		/// @brief FINALLY !!!!!
		void drawFrame();
		/// @brief Clean up the resources created for the app.
		void cleanup();
		/// @brief Remove the surface.
		void invalidateSurface() { this->surface = VK_NULL_HANDLE; }
		/// @bried Resizes all contents of this BasicApp.
		void resizeAll(int width, int height);
		/// @brief Fetches the current VkInstance.
		VkInstance getInstance() { return this->instance; }
		/// @brief Sets the VKSurfaceKHR to present information to the screen
		void setSurface(VkSurfaceKHR surface);
		/// @brief Set some initial sizes for the surface to draw on, to initialize the swapchain.
		void setInitialSizes(int w, int h);
		/// @brief Wait the physical device to be idle. Should only be called when closing the window.
		void waitIdle();
	protected:
		/// @brief Initialize the window, in order to render to it later.
		void initWindow();
		/// @brief Create the vulkan instance to use in this application.
		void createVulkanInstance();
		/// @brief Create a debug messenger for the current VkInstance.
		void setupDebugMessenger();
		/// @brief Picks a physical device suitable for rendering.
		void pickPhysicalDevice();
		/// @brief Creates a logical device from a physical one.
		void createLogicalDevice();
		/// @brief Get the queues available on the logical/physical device created.
		void getQueues();
		/// @brief Creates the swap chain
		void createSwapchain();
		/// @brief Creates the imageview needed for displaying an image on screen
		void createImageViews();

		uint32_t findMemoryType(uint32_t filter, VkMemoryPropertyFlags properties);

		/// @brief Creates the vertex buffers we'll use for this application
		void createVertexBuffer();
		/// @brief Creates the index buffer to draw the vertices in the right order.
		void createIndexBuffer();
		/// @brief Creates the uniform buffers, one per swapchain image.
		void createUniformBuffers();

		/// @brief Creates a render pass for the application
		void createRenderPass();

		/// @brief Creates the descriptor set for the graphics pipeline. In our case, that'll be 3 mat4s (MVP matrices)
		void createDescriptorSetLayout();
		/// @brief Creates the descriptor pool that the render will use
		void createDescriptorPool();
		/// @brief Creates the descriptor sets stored in the descriptor pool
		void createDescriptorSets();

		/// @brief Creates the graphics pipeline object.
		void createGraphicsPipeline();

		/// @brief Creates the framebuffers :
		void createFramebuffers();
		/// @brief Create the command pool to render an object
		void createCommandPool();
		/// @brief Create the graphics command buffers
		void createCommandBuffers();
		/// @brief create the fucking semaphore before drawing ...
		void createSemaphores();

		/// @brief Create a secondary thread which modifies the data uploaded to the VkInstance/VkDevice/VkLogicalDevice
		void spawnOptionalThread();

		/// @brief Generic function to create a buffer, given a few parameters.
		/// @param _buffer The buffer to create
		/// @param _buffer_memory The buffer memory contents
		/// @param _buffer_memory_properties The required memory properties for the buffer
		/// @param _buffer_size The buffer size, in bytes
		/// @param _buffer_usage The usage flags of the buffer
		void createBuffer(VkDeviceSize _buffer_size, VkBufferUsageFlags _buffer_usage,
						  VkMemoryPropertyFlags _buffer_memory_properties,
						  VkBuffer& _buffer, VkDeviceMemory& _buffer_memory);

		/// @brief Create an image object, given a few parameters
		/// @param width The width of the image, in pixels
		/// @param height The height of the image, in pixels
		/// @param mip_levels The number of mip levels to create
		/// @param _img The image object to create
		/// @param _img_format The format of the image to create
		/// @param _img_tiling The tiling mode of the image
		/// @param _img_usage The usage patterns of the image
		/// @param _img_memory The image memory store to allocate/bind
		/// @param _img_properties Properties the image memory should have
		void createImage(uint32_t width, uint32_t height, uint32_t mip_levels, VkSampleCountFlagBits _sample_count,
						 VkFormat _img_format, VkImageTiling _img_tiling, VkImageUsageFlags _img_usage,
						 VkMemoryPropertyFlags _img_properties, VkImage& _img, VkDeviceMemory& _img_memory);

		/// @brief Creates the image view from the image, given a specific format
		/// @param _img The image to create a view from
		/// @param _img_format The format of the image
		/// @param _img_aspect The aspect/expected use of the image
		/// @param mip_levels The number of mip levels to generate
		/// @returns The image view of the given image.
		VkImageView createImageView(VkImage _img, VkFormat _img_format, VkImageAspectFlags _img_aspect, uint32_t mip_levels);

		/// @brief Copies data from one (src) buffer to another (dst) buffer, when located on different devices.
		void copyBuffer(VkBuffer _src_buffer, VkBuffer _dst_buffer, VkDeviceSize _buffer_size);

		/// @b Updates the uniform buffers for the image currently in the swapchain
		void updateUniformBuffers(uint32_t image_in_swapchain);

		/// @brief Destroy and cleanup the current swapchain
		void destroySwapchain();
		/// @brief Reconstruct the swapchain under certain events (resize etc.)
		void reconstructSwapchain();

		/// @brief Create a texture buffer
		void createTextureImage();
		/// @brief Creates the texture view that can be sampled later
		void createTextureView();
		/// @brief Creates the texture sampler for this application's texture
		void createTextureSampler();

		/// @brief Create depth buffer, and its view/memory
		void createDepthBuffer();

		/// @brief Creates the MSAA image, used as a render target
		void createMSAARenderTarget();

		VkFormat findSupportedFormat(const std::vector<VkFormat>& candidates, VkImageTiling tiling, VkFormatFeatureFlags features);
		VkFormat findDepthFormat();

		bool has_stencil_component(VkFormat _format) {
			return _format == VK_FORMAT_D32_SFLOAT_S8_UINT || _format == VK_FORMAT_D24_UNORM_S8_UINT;
		}

		/// @brief Returns a valid, begun command buffer from which we can record one-time commands.
		/// @see endOneTImeCommands()
		VkCommandBuffer beginOneTimeCommands();

		/// @brief Ends the buffer passed in argument, and immediately submits the commands recorded to it.
		void endOneTimeCommands(VkCommandBuffer buffer);

		/// @brief Handles the transition between one image state and another
		void transitionImageLayout(VkImage _img, VkFormat _img_format, VkImageLayout _old_layout, VkImageLayout _new_layout, uint32_t mip_levels);

		/// @brief Copies a buffer into an image object
		void copyBufferToImage(VkBuffer _src_buf, VkImage _dst_img, uint32_t width, uint32_t height);

		/// @brief Generate mipmaps for the given image
		void generateMipMaps(VkImage _img, VkFormat _img_format, int32_t tex_width, int32_t tex_height, uint32_t mip_levels);

		/// @brief Load the model
		void loadModel();

		VkSampleCountFlagBits getMaxUsableSampleCounts();
	protected:
		VkSurfaceFormatKHR chooseSwapchainSupport(const Vulkan::DeviceFilter::PhyDev& dev);
		VkPresentModeKHR chooseSwapchainPresentMode(const Vulkan::DeviceFilter::PhyDev& dev);
		VkExtent2D chooseSwapchainExtent(const Vulkan::DeviceFilter::PhyDev& dev);
		VkShaderModule createShaderModule(const std::vector<char>& bytecode);
	private:
		/// @brief Dabug callback for the application
		static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
			VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
			VkDebugUtilsMessageTypeFlagsEXT messageType,
			const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
			void* pUserData);
		void createDebugInfoStruct(VkDebugUtilsMessengerCreateInfoEXT& createInfo);

	public:
		bool fbResized;
	private:
		int width, height;
		std::size_t currentFrame;
		std::vector<const char*> requiredLayers; ///< User-required layers
		std::vector<const char*> deviceExtensions; ///< Device layers to enable
		GLFWwindow* appWindow; ///< Window for the app
		bool enabledDebug; ///< Tracks if the debug layer should be enabled
		std::shared_ptr<Vulkan::DeviceFilter> devFilter; ///< The device filter used for physical/logical devices.
		std::shared_ptr<Vulkan::DeviceFilter::PhyDev> phyDev; ///< Physical device and all associated resources.
		VkInstance instance; ///< Vulkan instance
		VkPhysicalDevice physicalDevice; ///< The physical device used with the instance
		VkDevice logicalDevice; ///< The logical device created after picking the physical device
		VkDebugUtilsMessengerEXT debugMessenger; ///< The debug messenger, used to print Vulkan messages.
		VkQueue graphicsQueue;
		VkQueue presentQueue;
		VkSurfaceKHR surface;
		VkSwapchainKHR swapchain;
		std::vector<VkImage> swapchainImages;
		VkFormat swapchainImageFormat;
		VkExtent2D swapchainExtent;
		std::vector<VkImageView> swapchainImageViews;
		VkRenderPass renderPass;
		VkDescriptorSetLayout descriptorSetLayout;
		VkPipelineLayout pipelineLayout;
		VkPipeline pipeline;
		VkDescriptorPool descriptorPool;
		std::vector<VkDescriptorSet> descriptorSets;
		std::vector<VkFramebuffer> swapchainFramebuffers;
		VkCommandPool commandPool;
		std::vector<VkCommandBuffer> commandBuffers;
		VkBuffer vertexBuffer; ///< Holds the vertex specification and attribute bindings
		VkDeviceMemory vertexBufferMemory;
		VkBuffer indexBuffer;
		VkDeviceMemory indexBufferMemory;
		std::vector<VkBuffer> uniformBuffers;
		std::vector<VkDeviceMemory> uniformBuffersMemory;

		VkSampleCountFlagBits msaaSamples;

		std::vector<Vertex> vertices;
		std::vector<uint32_t> vert_indices;

		uint32_t mipLevels;					///< The mip levels of the available image
		VkImage textureImage;				///< Image representation of the loaded texture
		VkSampler textureSampler;			///< The sampler, attached to the image view
		VkImageView textureImageView;		///< The image view, which we can sample !
		VkDeviceMemory textureImageMemory;	///< Memory space of the image object

		VkImage depthImage;					///< The depth buffer
		VkImageView depthImageView;			///< The depth buffer's view
		VkDeviceMemory depthImageMemory;	///< The depth buffer's data

		VkImage colorImage;					///< The color target in MSAA rendering
		VkImageView colorImageView;			///< The color target's view
		VkDeviceMemory colorImageMemory;	///< The color target's memory space

		std::vector<VkSemaphore> imageAvailableSemaphores;
		std::vector<VkSemaphore> renderFinishedSemaphores;
		std::vector<VkFence> flyingFences;
		std::vector<VkFence> imageFences;

		std::unique_ptr<std::thread> optional_modifying_thread;
};

static void fbResizeCallback(GLFWwindow* win, int width, int height);

#endif // VKINTRO_BASIC_APP_HPP_
