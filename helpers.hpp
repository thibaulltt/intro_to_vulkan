#ifndef VULKAN_TUTORIAL_HELPERS_HPP_
#define VULKAN_TUTORIAL_HELPERS_HPP_

#include "./vk.hpp"

#include <fstream>
#include <vulkan/vulkan_core.h>

static std::vector<char> readFile(const std::string path) {
	// Create and open file :
	std::ifstream file(path, std::ios::ate | std::ios::binary);
	if (!file.is_open()) {
		throw std::runtime_error("Failed to open file \""+path+"\" !");
	}

	// Allocate buffer with given size :
	std::size_t fileSize = static_cast<std::size_t>(file.tellg());
	std::vector<char> buffer(fileSize);

	// Rewind, read and close file :
	file.seekg(0);
	file.read(buffer.data(), fileSize);
	file.close();

	// return read bytes :
	return buffer;
}

#endif // VULKAN_TUTORIAL_HELPERS_HPP_
