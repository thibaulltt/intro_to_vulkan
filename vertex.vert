#version 450
#extension GL_ARB_separate_shader_objects : enable

/*
// When hardcoding vertex positions and colors in the VShader :
vec2 positions[3] = vec2[] (
	vec2( 0.0, -0.5),
	vec2( 0.5,  0.5),
	vec2(-0.5,  0.5)
);
vec3 colors[3] = vec3[] (
	vec3(1., .0, .0),
	vec3(0., 1., .0),
	vec3(0., .0, 1.)
);
layout(location=0) out vec3 vertColor;
*/

// Using a UBO-like layout descriptor set :
layout(binding=0) uniform UniformBufferObject {
	mat4 model;
	mat4 view;
	mat4 proj;
} ubo;
// note : we can specify a set by doing :
// > location(set=0, binding=0) ...
// for a descriptor set.

// When using a CPU-created buffer :
layout(location=0) in vec3 inPosition;
layout(location=1) in vec3 inColor;
layout(location=2) in vec2 texCoord;

layout(location=0) out vec3 fragColor;
layout(location=1) out vec2 fragTexCoord;

void main() {
	/*
	// When hardcoding vertex positions and colors in the VShader :
	gl_Position = vec4(positions[gl_VertexIndex], .0, 1.);
	vertColor = colors[gl_VertexIndex];
	*/

	// otherwise :
	gl_Position = ubo.proj * ubo.view * ubo.model * vec4(inPosition, 1.);
	fragColor = inColor;
	fragTexCoord = texCoord;
}
