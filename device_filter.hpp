#ifndef VK_DEVICE_FILTER_HPP
#define VK_DEVICE_FILTER_HPP

#include "vk.hpp"
#include <initializer_list>

#include <optional>
#include <vulkan/vulkan_core.h>

#ifndef VULKAN_CORE_H_
#	include <vulkan/vulkan_core.h>
#endif

namespace Vulkan {

	namespace Device {

		// Possible device features, as of Vulkan 1.2.157 (VERSION NOT CHECKED @ RUN/COMPILE) {{{
		enum Features {
			robustBufferAccess = 0,
			fullDrawIndexUint32,
			imageCubeArray,
			independentBlend,
			geometryShader,
			tessellationShader,
			sampleRateShading,
			dualSrcBlend,
			logicOp,
			multiDrawIndirect,
			drawIndirectFirstInstance,
			depthClamp,
			depthBiasClamp,
			fillModeNonSolid,
			depthBounds,
			wideLines,
			largePoints,
			alphaToOne,
			multiViewport,
			samplerAnisotropy,
			textureCompressionETC2,
			textureCompressionASTC_LDR,
			textureCompressionBC,
			occlusionQueryPrecise,
			pipelineStatisticsQuery,
			vertexPipelineStoresAndAtomics,
			fragmentStoresAndAtomics,
			shaderTessellationAndGeometryPointSize,
			shaderImageGatherExtended,
			shaderStorageImageExtendedFormats,
			shaderStorageImageMultisample,
			shaderStorageImageReadWithoutFormat,
			shaderStorageImageWriteWithoutFormat,
			shaderUniformBufferArrayDynamicIndexing,
			shaderSampledImageArrayDynamicIndexing,
			shaderStorageBufferArrayDynamicIndexing,
			shaderStorageImageArrayDynamicIndexing,
			shaderClipDistance,
			shaderCullDistance,
			shaderFloat64,
			shaderInt64,
			shaderInt16,
			shaderResourceResidency,
			shaderResourceMinLod,
			sparseBinding,
			sparseResidencyBuffer,
			sparseResidencyImage2D,
			sparseResidencyImage3D,
			sparseResidency2Samples,
			sparseResidency4Samples,
			sparseResidency8Samples,
			sparseResidency16Samples,
			sparseResidencyAliased,
			variableMultisampleRate,
			inheritedQueries
		};
		/// }}}

		// Possible device types {{{
		enum Types {
			Other,	// Other device types
			iGPU,	// Integrated GPU
			dGPU,	// Discrete/Dedicated GPU
			vGPU,	// Virtual GPU
			CPU	// CPU
		};
		// }}}

	}

	// @brief Allows to filter the device-local physical devices, to create a Vulkan instance.
	// @details Allows to filter by extensions, and layers available. The user can specify a
	// set of *required* and *optional* extensions and layers, in order to get the proper
	// physical device for a Vulkan Instance.
	class DeviceFilter {
		public:
			// Queue families supported by the device {{{
			struct QueueFamilies {
				std::optional<uint32_t> graphicsFamily;
				std::optional<uint32_t> computeFamily;
				std::optional<uint32_t> presentFamily;
				bool isValid() const {
					return this->graphicsFamily.has_value() &&
						this->presentFamily.has_value() &&
						this->computeFamily.has_value();
				}
			};
			// }}}
			// Swapchain support {{{
			struct SwapchainSupportDetails {
				VkSurfaceCapabilitiesKHR capabilities;
				std::vector<VkSurfaceFormatKHR> formats;
				std::vector<VkPresentModeKHR> presentMode;
			};
			// }}}
			// Structure holding a PhysicalDevice and its score {{{
			struct PhyDev {
				const VkPhysicalDevice *const device;
				VkPhysicalDeviceProperties props;
				VkPhysicalDeviceFeatures feats;
				QueueFamilies queues;
				SwapchainSupportDetails details;
				float score;
			};
			// }}}
		public:
			/// @brief Create a device filter, for the given physical devices.
			DeviceFilter(const std::vector<VkPhysicalDevice>&, const VkSurfaceKHR& surface);
			/// @brief Destroy the device filter, and free the allocated memory.
			~DeviceFilter(void);

			/// @brief Add a feature *needed* in the device.
			DeviceFilter& addRequestedFeatures(std::initializer_list<Device::Features>);

			// TODO : implement this :
			/// @brief Add requested properties.
			DeviceFilter& addRequestedProperties(std::initializer_list<float>);

			/// @brief Adds the device extensions required
			DeviceFilter& addDeviceExtensions(std::initializer_list<const char*> ext);
			/// @brief Adds the device extensions required
			DeviceFilter& addDeviceExtensions(std::vector<const char*> ext);

			/// @brief Adds the required surface formats
			DeviceFilter& addSurfaceFormats(std::initializer_list<VkSurfaceFormatKHR> fmt);
			DeviceFilter& addPresentModes(std::initializer_list<VkPresentModeKHR> pMode);
			DeviceFilter& setWindowsize(VkExtent2D size);

			/// @brief Sets the required device type for a physical device.
			DeviceFilter& setRequestedDeviceType(Device::Types);

			/// @brief Get the best device, according to the current features & properties requested.
			PhyDev& getBestDevice(std::vector<const char*> extensionsrequired);
		/// @brief Query support details.
		SwapchainSupportDetails swapchainSupport(const VkPhysicalDevice& dev, const VkSurfaceKHR& surf, bool printValues = false);
		protected:
			/// @brief Get the physical properties of the device queried.
			VkPhysicalDeviceProperties getPhysicalDeviceProperties(const VkPhysicalDevice& dev, bool printValues = false);
			/// @brief Get the existing features of the device queried.
			VkPhysicalDeviceFeatures getPhysicalDeviceFeatures(const VkPhysicalDevice& dev, bool printValues = false);
			/// @brief Prints the available queue families per-device.
			QueueFamilies getAvailableQueues(const VkPhysicalDevice& dev, const VkSurfaceKHR& surface, bool printValues = false);
			/// @brief Returns true if the surface formats are available
			bool hasAllSurfaceFormats(const PhyDev& dev);
			/// @brief Returns true if the present modes are available
			bool hasAllPresentModes(const PhyDev& dev);
			bool canFitWindowSize(const PhyDev& dev);
		protected:
			VkExtent2D winSize;
			std::vector<const char*> extensions; ///< Enabled extensions to require
			std::vector<VkSurfaceFormatKHR> formats;///< Allowed surface formats
			std::vector<VkPresentModeKHR> presentMode; ///< Allowed present modes
			std::vector<PhyDev> physicalDevices; ///< The physical devices to filter.
			std::vector<PhyDev> suitableDevices; ///< The suitable devices.
			VkPhysicalDeviceType requiredType; ///< Required device type
			VkPhysicalDeviceFeatures userFeatures; ///< The user-required or user-optional features.
			VkPhysicalDeviceProperties requiredProperties; ///< The required properties of the device.
			VkPhysicalDeviceProperties optionalProperties; ///< The optional properties of the device.
	};
}

#endif // VK_DEVICE_FILTER_HPP
