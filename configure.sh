#!/bin/bash

GLSLC=$(which glslc)

# Check for GLSL compiler :
if [ -z $GLSLC ]; then
	echo -e "Could not find GLSL compiler (glslc) in path."
	echo -e "Your current PATH is :\"\n$PATH\n\""
	exit 1 # return with error
else
	echo "Found GLSL compiler at $GLSLC"
fi

# Create shaders directory if not available :
if [[ ! -d ./shaders/ ]]; then
	mkdir ./shaders
	echo "Created shaders directory."
fi

$GLSLC vertex.vert -o shaders/vert.spv
$GLSLC fragment.frag -o shaders/frag.spv
