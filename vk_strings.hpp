#ifndef VK_STRINGS_HPP
#define VK_STRINGS_HPP
#pragma once

#include "vk.hpp"

#include <array>

#ifndef VULKAN_CORE_H_
#	include <vulkan/vulkan_core.h>
#endif

namespace Vulkan {

	using Version = std::array<uint32_t, 3>;

	Version getVulkanVersion(uint32_t packed);

	std::string versionToString(Version v);

namespace Strings {

	// Error codes, as of Vulkan 1.2.157 {{{
	struct errors {
		VkResult result;
		std::string str;
	};
	extern std::vector<errors> errorCodes; ///< Stores all the error code <-> string associations.
	std::string getResultType(VkResult _result); ///< Allows to get a human-readable version of an error code.
	// }}}

	// Message severity bits, as of Vulkan 1.2.157 {{{
	struct severity {
		VkDebugUtilsMessageSeverityFlagBitsEXT level;
		std::string str;
	};
	extern std::vector<severity> messageSeverities;
	std::string getMessageSeverity(VkDebugUtilsMessageSeverityFlagBitsEXT sev);
	// }}}

	// Message types, as of Vulkan 1.2.157 {{{
	struct messagetypes {
		VkDebugUtilsMessageTypeFlagsEXT type;
		std::string str;
	};
	extern std::vector<messagetypes> messageTypes;
	std::string getMessageType(VkDebugUtilsMessageTypeFlagsEXT t);
	// }}}

	// Object types, as of Vulkan 1.2.157 {{{
	struct objects {
		VkObjectType type;
		std::string str;
	};
	extern std::vector<objects> objectTypes;
	std::string getObjectType(VkObjectType objType);
	// }}}

	// Physical device types, as of Vulkan 1.2.157 {{{
	struct physicaldevicetype {
		VkPhysicalDeviceType type;
		std::string str;
	};
	extern std::vector<physicaldevicetype> physicalDeviceTypes;
	std::string getPhysicalDeviceType(VkPhysicalDeviceType t);
	// }}}

	/***************************************************/
	/***************************************************/
	/**************       PRINTERS       ***************/
	/***************************************************/
	/***************************************************/

	// @brief Print the error code, from a VkResult value
	void printResultType(VkResult _result);

	// printMessageSeverity() : Get the debug callback's severity type as a string
	void printMessageSeverity(VkDebugUtilsMessageSeverityFlagBitsEXT sev);

	// printMessageType() : Get the debug callback's message type as a string
	void printMessageType(VkDebugUtilsMessageTypeFlagsEXT t);

	// printObjectType() : Get an object's type (in readable format) from its value type
	void printObjectType(VkObjectType objType);

	// printPhysicalDeviceType() : Get the human-readable type of a physical device
	void printPhysicalDeviceType(VkPhysicalDeviceType t);
}
}

#endif // VK_STRINGS_HPP

