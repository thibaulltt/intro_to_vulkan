#ifndef VKTESTING_VULKAN_HEADER_HPP
#define VKTESTING_VULKAN_HEADER_HPP

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <glm/mat4x4.hpp>

#ifndef VULKAN_H_
#	include <vulkan/vulkan.h>
#	include <vulkan/vulkan_core.h>
#endif

#include <iostream>
#include <stdexcept>
#include <cstdlib>
#include <vector>
#include <string.h>
#include <algorithm>
#include <initializer_list>

// User-definable macros {{{
#define PRINT_SEPARATOR(fname) std::cerr << "======================= "\
	<< fname << " =======================\n"
#define PRINT_FN PRINT_SEPARATOR(__FUNCTION__)
// }}}

/// @brief Namespace where all vulkan code will reside
namespace Vulkan {}

#endif // VKTESTING_VULKAN_HEADER_HPP
