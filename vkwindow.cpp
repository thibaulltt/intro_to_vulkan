#include "vkwindow.hpp"
#include "basic_app.hpp"
#include <qevent.h>
#include <qwindow.h>

VKWindow::VKWindow(QWindow* parent): vkapp(), _isInitialized(false), QWindow(parent) {
	this->setSurfaceType(QSurface::VulkanSurface);
	this->vkapp.init();
}

void VKWindow::close() {
	this->vkapp.waitIdle();
	this->vkapp.cleanup();
	this->vkapp.invalidateSurface();
	this->QWindow::close();
}

void VKWindow::vkUpdate() {
	if (not this->_isInitialized) {
		std::cerr << "INIT ALL!!!\n";
		QVulkanInstance* vkinstance = new QVulkanInstance();
		vkinstance->setVkInstance(this->vkapp.getInstance());
		vkinstance->create();
		this->setVulkanInstance(vkinstance);

		int w = this->width();
		int h = this->height();
		if (w==0 or h==0) {
			// Might happen on next draw...
			this->resize(QSize(WIN_WIDTH, WIN_HEIGHT));
			w = WIN_WIDTH;
			h = WIN_HEIGHT;
		}
		this->vkapp.setInitialSizes(w, h);
		this->vkapp.setSurface(vkinstance->surfaceForWindow(this));
		this->vkapp.initVulkan();
		this->_isInitialized = true;
	}
	this->vkapp.drawFrame();
	this->requestUpdate();
}

bool VKWindow::event(QEvent* event) {
	if (this->isExposed() and event->type() == QEvent::Type::UpdateRequest) {
		this->vkUpdate();
	}
	if (this->isExposed() and event->type() == QEvent::Type::PlatformSurface) {
		auto pse = static_cast<QPlatformSurfaceEvent*>(event);
		if (pse != nullptr) {
			if (pse->surfaceEventType() == QPlatformSurfaceEvent::SurfaceAboutToBeDestroyed) {
				std::cerr << "The surface is about to be destroyed!\n" ;
				this->vkapp.waitIdle();
				this->vkapp.cleanup();
				this->vkapp.invalidateSurface();
				this->_isInitialized = false;
			}
			else if (pse->surfaceEventType() == QPlatformSurfaceEvent::SurfaceCreated) {
				std::cerr << "PLATFORM INIT!!!\n";
			}
		}
	}
	return QWindow::event(event);
}

void VKWindow::exposeEvent(QExposeEvent* event) {
	//this->QWindow::exposeEvent(event);
	if (this->isExposed()) {
		this->vkUpdate();
	}
}

void VKWindow::resizeEvent(QResizeEvent* ev) {

	const QSize os = ev->oldSize();
	const int ow = os.width();
	const int oh = os.height();
	const QSize ns = ev->size();
	const int w = ns.width();
	const int h = ns.height();
	if (ow == 0 or oh == 0) {
		std::cerr << "resizeEvent() ignored... was the first one.\n";
	} else {
		std::cout << "resizeEvent(): :" << ow << "x" << oh << " --> " << w << "x" << h << "!!!\n";
		this->vkapp.resizeAll(w, h);
	}
	return this->QWindow::resizeEvent(ev);
}

void VKWindow::closeEvent(QCloseEvent *e) {
	if (this->_isInitialized) {
		this->vkapp.waitIdle();
		this->vkapp.cleanup();
		this->vkapp.invalidateSurface();
	}
	this->QWindow::closeEvent(e);
}

