#ifndef VK_INTRO_VK_WINDOW_HPP_
#define VK_INTRO_VK_WINDOW_HPP_

#include "basic_app.hpp"

#include <QEvent>
#include <QPaintEvent>
#include <QVulkanInstance>
#include <QWindow>

class VKWindow: public QWindow {

	public:
		VKWindow(QWindow* parent = nullptr);
		virtual ~VKWindow() = default;

	public slots:
		void close();
		void vkUpdate();

	protected:
		bool event(QEvent* e) override;
		void exposeEvent(QExposeEvent* e) override;
		void resizeEvent(QResizeEvent* e) override;
		void closeEvent(QCloseEvent* e) override;

	protected:
		BasicApp vkapp;
		bool _isInitialized;
};

#endif // VK_INTRO_VK_WINDOW_HPP_
