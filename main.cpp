/** [Started on 16th September, 2020]
 * Intro to vulkan, really superficial {{{
 *
 * WHAT IT TAKES TO DRAW A TRIANGLE
 * ================================
 *
 * - Need to create a Vulkan Instance [VkInstance]
 *   - Describes the app, and extensions used
 *   - Allows to pick a physical device in order to render something to the screen [VkPhysicalDevice]
 * - Create a logical device handle [VkDevice]
 *   - Defines what features used, and queue families used [VkQueue(s)]
 *     - [VkQueue] can have orders submitted asynchronously (!)
 *       - Operations on a [VkQueue] must be submitted using a [VkCommandBuffer], allocated from a [VkCommandPool]
 *         - Exemple of a command buffer to draw a triangle :
 *           - Begin render pass,
 *           - Bind the graphics pipeline,
 *           - Draw 3 vertices,
 *           - End the render pass.
 * - Create a window surface [VkSurfaceKHR] & swap chain [VkSwapchainKHR]
 *   - Swap chain is a collection of render targets (e.g. to have double buffering)
 *   - Every render, we need to request an image to swap chain, to render to, returned to the swap chain
 *     to show on screen
 *   - Every image created by the swap chain is wrapped in a [VkImageView] and a [VkFramebuffer] (framebuffer
 *     can have multiple image views for color, stencil and depth).
 * - Define render passes (defines how to use images and their types in the rendering process)
 *   - Render pass only describes types of images, a [VkFramebuffer] binds specific images to slots
 * - Define a rendering pipeline [VkPipeline] ***in advance*** of rendering
 *   - [VkPipeline] defines the GPU's configurable state (viewport size, depth buffer operation ...)
 *   - [VkPipeline] also defines [VkShaderModule] (created by shader's bytecode once compiled)
 *   - WARNING : every change required a rebuilding of the entire [VkPipeline]
 *
 *  Once physical devices chosen, pipeline created, and commands recorded, to render we acquire the next image
 *  from the swap chain [vkAcquireNextImageKHR()], select the command buffer for the image, and execute it
 *  with [vkQueuSubmit()].
 *  Since those can be sumitted asynchronously, need to use synchronisation objects (aka semaphores) to wait
 *  for specific timepoints (render on an image once the image has been acquired from the framebuffer, for
 *  exemple).
 *
 * }}}
 */

#include "./vk.hpp"
#include "./basic_app.hpp"
#include "./vkwindow.hpp"

#include <iostream>
#include <QGuiApplication>
#include <qwindow.h>

int main(int argc, char* argv[])  {
	QGuiApplication app(argc, argv);

	VKWindow w;
	//w.resize(WIN_WIDTH, WIN_HEIGHT);
	w.show();

	return app.exec();
}
